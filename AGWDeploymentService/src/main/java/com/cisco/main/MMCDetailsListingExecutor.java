/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.properties.CredentialDecoder;

public class MMCDetailsListingExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		Map<String, String> mmchostlist = new HashMap<String, String>();
		Map<String, String> mmcportlist = new HashMap<String, String>();
		Map<String, String> mmcauthlist = new HashMap<String, String>();
		// CredentialDecoder decoder = new CredentialDecoder();
		Properties data = propload();
		for (String key : data.stringPropertyNames()) {
			if (key.contains("_mmchost")) {
				String value = data.getProperty(key);
				if (!value.equalsIgnoreCase(""))
					mmchostlist.put(key, value);
			}
			if (key.contains("_mmcport")) {
				String value = data.getProperty(key);
				if (!value.equalsIgnoreCase(""))
					mmcportlist.put(key, value);
			}
			if (key.contains("_mmcauth")) {
				String value = data.getProperty(key);
				if (!value.equalsIgnoreCase("")) {
					// String cred = decoder.getCredential(value);
					mmcauthlist.put(key, value);
				}
			}
		}
		System.out.println("com.cisco.main.MMCDetailsListingExecutor class :: host Map---->" + mmchostlist);
		System.out.println("com.cisco.main.MMCDetailsListingExecutor class :: port Map---->" + mmcportlist);
		System.out.println("com.cisco.main.MMCDetailsListingExecutor class :: auth Map---->" + mmcauthlist);
		arg0.getMessage().setProperty("mmchostnamelist", mmchostlist, PropertyScope.SESSION);
		arg0.getMessage().setProperty("mmcportlist", mmcportlist, PropertyScope.SESSION);
		arg0.getMessage().setProperty("mmcauthlist", mmcauthlist, PropertyScope.SESSION);

		return arg0.getMessage();
	}

	public Properties propload() {
		Properties prop = new Properties();
		// InputStream input = null;

		try {

			// input = new FileInputStream("/mule-app.properties");
			String env = System.getProperty("mule.env");
			System.out.println("ENV----" + env);

			// load a properties file
			prop.load(this.getClass().getResourceAsStream("/config." + env + ".properties"));

		} catch (IOException ex) {
			System.out.println("com.cisco.main.MMCDetailsListingExecutor class :: IOException on property loading");
			ex.printStackTrace();
		} finally {
			// if (input != null) {
			// try {
			// input.close();
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// }
		}

		return prop;

	}

}
