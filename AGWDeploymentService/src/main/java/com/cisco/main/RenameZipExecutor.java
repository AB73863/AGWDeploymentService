/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class RenameZipExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String PackageFolderLocation = arg0.getMessage().getOutboundProperty(
				"PackageFolderLocation");
		String myFilename = FilenameUtils.getBaseName(PackageFolderLocation);
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String deploymentname = arg0.getMessage().getInvocationProperty(
				"deploymentname");

		File oldfile = new File(drive + baseUrl + myFilename + ".zip");
		File newfile = new File(drive + baseUrl + deploymentname + ".zip");

		if (oldfile.renameTo(newfile)) {
			System.out.println("com.cisco.main.RenameZipExecutor class :: Rename succesful");
		} else {
			System.out.println("com.cisco.main.RenameZipExecutor class :: Rename failed");
		}
		return arg0.getMessage();
	}

}
