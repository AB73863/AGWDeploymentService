/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.exception.ValidationOverrideException;

public class OverrideActionExceutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String domain = arg0.getMessage().getOutboundProperty("Domain");
		String configFileName = domain + "-" + "dev" + ".config.properties";
		String configFileLocation = arg0.getMessage().getProperty(
				"ConfigFileLocation", PropertyScope.INVOCATION);
		String destconfigDirectory = drive + configFileLocation + domain + "/";

		// String lifecycle = arg0.getMessage().getOutboundProperty(
		// "Lifecycle");
		// String domain = arg0.getMessage().getOutboundProperty(
		// "Domain");
		// String drive = arg0.getMessage().getProperty("Drive",
		// PropertyScope.INVOCATION);
		// String configFileLocation =
		// arg0.getMessage().getProperty("ConfigFileLocation",
		// PropertyScope.INVOCATION);
		// String configFileName =
		// domain+"-"+lifecycle.toLowerCase()+".config.properties";
		// String constconfigFileLoc = drive+configFileLocation+domain+"/";
		// System.out.println("configFileName-->"+configFileName);
		// System.out.println("constconfigFileLoc-->"+constconfigFileLoc);
		// arg0.getMessage().setProperty("Domain", domain,
		// PropertyScope.OUTBOUND);
		// arg0.getMessage().setProperty("ConfigFileName", configFileName,
		// PropertyScope.OUTBOUND);
		// arg0.getMessage().setProperty("ConfigFileLocation",
		// constconfigFileLoc, PropertyScope.OUTBOUND);
		// arg0.getMessage().setProperty("ExecutionGroupName", domain,
		// PropertyScope.OUTBOUND);

		System.out.println("com.cisco.main.OverrideActionExecutor class :: drive :" + drive + "domain ::" + domain
				+ "configFileLocation ::" + configFileLocation
				+ "destconfigDirectory ::" + destconfigDirectory
				+ "config-filename ::" + configFileName);
		File destfile = new File(destconfigDirectory + configFileName);
		InputStream input = null;
		if (destfile.exists()) {

			System.out.println("com.cisco.main.OverrideActionExecutor class :: The file is available!!!");
			Properties properties = new Properties();
			input = new FileInputStream(destconfigDirectory + configFileName);
			// load a properties file
			try {
				if (input.available() > 0) {
					System.out.println(" com.cisco.main.OverrideActionExecutor class :: bytes :" + input.available());
					properties.load(input);
				} else {
					System.out.println("com.cisco.main.OverrideActionExecutor class :: No prop available");
					input.close();
					throw new ValidationOverrideException();
				}
				System.out.println("com.cisco.main.OverrideActionExecutor class :: validationoverride-->"
						+ properties.getProperty("validationoverride"));
				
				System.out.println("com.cisco.main.OverrideActionExecutor class :: repackageoverride-->"
						+ properties.getProperty("repackageoverride"));
				if (properties.getProperty("validationoverride") == null || properties.getProperty("validationoverride").isEmpty()) {
					System.out.println("com.cisco.main.OverrideActionExecutor class :: No prop available");
					input.close();
					throw new ValidationOverrideException();
				} else
					arg0.getMessage().setProperty("validationoverride",
							properties.getProperty("validationoverride"),
							PropertyScope.OUTBOUND);
				// For repack overrride
				if (properties.getProperty("repackageoverride") == null || properties.getProperty("repackageoverride").isEmpty()) {
					System.out.println("No prop available");
					input.close();
					throw new ValidationOverrideException();
				} else
					arg0.getMessage().setProperty("repackageoverride",
							properties.getProperty("repackageoverride"),
							PropertyScope.OUTBOUND);
			}finally {
				input.close();
			}
		}else{
			System.out.println("com.cisco.main.OverrideActionExecutor class :: No prop available");
			//input.close();
			throw new ValidationOverrideException();
		}
		return arg0.getMessage();
	}

}
