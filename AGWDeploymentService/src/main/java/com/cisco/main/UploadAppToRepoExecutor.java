/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;
/**
 * @author arubalac
 */
import java.util.HashMap;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.deployment.Deployment;

public class UploadAppToRepoExecutor implements Callable{

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String PackageFolderLocation = arg0.getMessage().getOutboundProperty(
				"PackageFolderLocation");
		String filename = arg0.getMessage().getOutboundProperty(
				"myfilename");
		String mmchost = arg0.getMessage().getProperty("mmchost", PropertyScope.SESSION);
		String mmcport = arg0.getMessage().getProperty("mmcport", PropertyScope.SESSION);
		String mmcauth = arg0.getMessage().getProperty("mmcauth", PropertyScope.SESSION);
		String mmcpath = arg0.getMessage().getMuleContext().getRegistry().get("addToRepo");
		System.out.println("mmcpath ::"+ mmcpath);
		System.out.println("Packagefolderloc ::"+PackageFolderLocation+"filename ::"+filename);
		Deployment deployment = new Deployment();
		String ApplicationverId = deployment.uploadAppToRepo(PackageFolderLocation, filename, "1.0",mmchost,mmcport,mmcpath,mmcauth);
		System.out.println("com.cisco.main.UploadAppToRepoExecutor class :: appid--------->"+ApplicationverId);
		Map<String, Object> map = new HashMap<>();
		map.put("ApplicationverId", ApplicationverId);
		arg0.getMessage().addProperties(map, PropertyScope.OUTBOUND);

		return arg0.getMessage();
	}

}
