/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 * 
 */
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.properties.DomainAndEnvEditor;

public class DomainEnvEditExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String domain = arg0.getMessage().getOutboundProperty("Domain");
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String filename = arg0.getMessage().getOutboundProperty("myFilename");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);

		String deploypropDirectory = drive + baseUrl + filename
				+ "/mule-deploy.properties";
		String apppropDirectory = drive + baseUrl + filename
				+ "/mule-app.properties";
		String repackoverride = arg0.getMessage().getOutboundProperty("repackageoverride");
		if(repackoverride.equalsIgnoreCase("false")){
		DomainAndEnvEditor domainenveditor = new DomainAndEnvEditor();
		domainenveditor.setDomain(domain, deploypropDirectory);
		domainenveditor.removeMuleEnv(apppropDirectory);
		}
		return arg0.getMessage();
	}

}
