/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class OrgAndAPIDetailExtractExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws IOException {
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String filename = arg0.getMessage().getOutboundProperty("myFilename");
		String env = arg0.getMessage().getOutboundProperty("Lifecycle");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String group = arg0.getMessage().getOutboundProperty("Domain")+"-group1";
		String destconfigDirectory = drive + baseUrl + filename + "/classes/"
				+ "config." + env.toLowerCase() + ".properties";
		String destsubscribeDirectory = drive + baseUrl + filename
				+ "/classes/" + "subscription." + env.toLowerCase()
				+ ".properties";
		System.out.println("com.cisco.main.OrgAndAPIDetailExtractExecutor class :: group--->"+ group);
		System.out.println("com.cisco.main.OrgAndAPIDetailExtractExecutor class :: dir1--->" + destconfigDirectory);
		System.out.println("com.cisco.main.OrgAndAPIDetailExtractExecutor class :: dir2--->" + destsubscribeDirectory);
		FileInputStream in_config;
		FileInputStream in_subscription;
		try {
			in_config = new FileInputStream(destconfigDirectory);
		} catch (FileNotFoundException e1) {
            System.out.println("com.cisco.main.OrgAndAPIDetailExtractExecutor class :: FileNotFound reading config property file");		
			throw new RuntimeException("config." + env.toLowerCase()
					+ ".properties not found in location : "
					+ destconfigDirectory);
		}
		Properties props_config = new Properties();
		try {
			props_config.load(in_config);
		} catch (IOException e) {
			System.out.println("com.cisco.main.OrgAndAPIDetailExtractExecutor class :: IOException on writting config property file in proxy");
			e.printStackTrace();
		}
		//String apiid = props_config.getProperty("api.id");
		String apiverison = props_config.getProperty("api.version");
		String apiname = props_config.getProperty("api.name");
		//arg0.getMessage().setInvocationProperty("Apiid", apiid);
		arg0.getMessage().setInvocationProperty("ApiVerison", apiverison);
		//using the ApiName get the ApiId
		arg0.getMessage().setInvocationProperty("ApiName", apiname);
		in_config.close();

		try {
			in_subscription = new FileInputStream(destsubscribeDirectory);
		} catch (FileNotFoundException e) {
			System.out.println("com.cisco.main.OrgAndAPIDetailExtractExecutor class :: FileNotFound while writting subscription property in proxy");
			throw new RuntimeException("subscritpion." + env.toLowerCase()
					+ ".properties not found in location : "
					+ destsubscribeDirectory);
		}
		Properties props_subs = new Properties();
		props_subs.load(in_subscription);
		String organizationid = props_subs.getProperty("organizationid");
		String groupname = props_subs.getProperty(group+".name");
		String requestsizelimit = props_subs.getProperty(group+".requestsizelimit");
		String responsesizelimit = props_subs.getProperty(group+".responsesizelimit");
		String responselatencylimit = props_subs.getProperty(group+".responselatencylimit");
		String tps = props_subs.getProperty(group+".tps");
		arg0.getMessage().setInvocationProperty("Organizationid",
				organizationid);
		arg0.getMessage().setInvocationProperty("Groupname",
				groupname);
		arg0.getMessage().setInvocationProperty("Requestsizelimit",
				requestsizelimit);
		arg0.getMessage().setInvocationProperty("Responsesizelimit",
				responsesizelimit);
		arg0.getMessage().setInvocationProperty("Responselatencylimit",
				responselatencylimit);
		arg0.getMessage().setInvocationProperty("Tps",
				tps);
		in_subscription.close();

		return arg0.getMessage();
	}

}
