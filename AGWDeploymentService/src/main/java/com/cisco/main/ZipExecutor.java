/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;
/**
 * @author arubalac
 */
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.zip.manage.ZipUtility;

public class ZipExecutor implements Callable{

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String filename = arg0.getMessage().getOutboundProperty("myFilename");
		
		System.out.println("com.cisco.ZipExecutor class :: location--->" +baseUrl + drive + filename);
		
		ZipUtility ziputil = new ZipUtility();
		String dirToZip = drive+baseUrl+filename;
		String zipFilepath = drive+baseUrl+filename+".zip"; 
		ziputil.zipFolder(dirToZip, zipFilepath);
        
		return arg0.getMessage();
	}
	
	

}
