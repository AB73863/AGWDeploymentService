/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.bean.SrvlResponse;
import com.cisco.bean.ValidationResponse;
import com.cisco.properties.PropertyFileValidator;
import com.cisco.validate.ProxyValidator;

public class ReviewExecutor implements Callable {

	public Object onCall(MuleEventContext eventContext) throws Exception {
		System.out.println("com.cisco.main.ReviewExecutor class :: Inside the proxy validator component");
		String proxybaseUrl = eventContext.getMessage().getOutboundProperty(
				"ProxybaseUrl");
		String baseUrl = eventContext.getMessage().getOutboundProperty(
				"baseUrl");
		String filename = eventContext.getMessage().getOutboundProperty(
				"myFilename");
		String drive = eventContext.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		//String env = eventContext.getMessage().getOutboundProperty(
		//		"Environment");
		String outsrvllocation = eventContext.getMessage().getProperty(
				"outsrvllocation",PropertyScope.INVOCATION);
		String shfilelocation = eventContext.getMessage().getProperty(
				"shfilelocation",PropertyScope.INVOCATION);
		String validationoverride = eventContext.getMessage()
				.getOutboundProperty("validationoverride");
		ProxyValidator obj = new ProxyValidator();
		SrvlResponse proxyvalidationstatus=null;
		System.out.println("com.cisco.main.ReviewExecutor class :: proxy xml location--->" + drive + baseUrl
				+ filename + "/proxy.xml");
		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		Date date = new Date();
		System.out.println("com.cisco.main.ReviewExecutor class :: " + dateFormat.format(date));
		// Read the schematron file and outputvalidation location from a
		// property file
		System.out.println("com.cisco.main.ReviewExecutor class :: srvllocation---->"+drive+outsrvllocation+filename+"_"+dateFormat.format(date)+".txt");
//		SrvlResponse proxyvalidationstatus = obj.review(drive+shfilelocation, drive
//				+ baseUrl + filename + "/proxy.xml",
//				drive+outsrvllocation+filename+"_"+dateFormat.format(date)+".txt", env.toLowerCase());
		//For validation override
		if(validationoverride.equalsIgnoreCase("false")){
			proxyvalidationstatus = obj.review(drive+shfilelocation, drive
					+ baseUrl + filename + "/proxy.xml",
					drive+outsrvllocation+filename+"_"+dateFormat.format(date)+".txt");
		}
		
		PropertyFileValidator propval = new PropertyFileValidator();
//		boolean propEnvstatus = propval.checkPropEnv(drive, baseUrl, filename,
//				env.toLowerCase());
		Map<String, Boolean> propEnvstatusmap = propval.checkPropEnv(drive, baseUrl, filename);

		boolean propOverallEnvstatus = false;
		boolean propOverallvalidationstatus = false;
		HashMap <String,Boolean> propvalidationlist = new HashMap<String,Boolean>();
		boolean propvalstatustemp = false;
		String propValFailedEnv;
		System.out.println("com.cisco.main.ReviewExecutor class :: propEnvstatusmap--->"+propEnvstatusmap.toString());
		System.out.println("com.cisco.main.ReviewExecutor class :: propCheckValueContains--->"+propEnvstatusmap.containsValue(true));
		propOverallEnvstatus = propEnvstatusmap.containsValue(true);
		
		// Check is the property file exist , if exist then check the content of
		// the property file
		
		if (propOverallEnvstatus)
			for (Entry<String, Boolean> entry : propEnvstatusmap.entrySet()) {
			    System.out.println("com.cisco.main.ReviewExecutor class :: env Key = " + entry.getKey() + ", status Value = " + entry.getValue());
			    if(entry.getValue()){
			    	propvalstatustemp = propval.propValidate(drive, baseUrl,filename, proxybaseUrl, entry.getKey().toLowerCase());
			    	propvalidationlist.put(entry.getKey(), propvalstatustemp);
			    }
			    propOverallvalidationstatus = !propvalidationlist.containsValue(false);
			}
		else{
			propvalstatustemp = propval.propValidateifNoEnv(drive, baseUrl,
					filename, proxybaseUrl);
			propOverallvalidationstatus = propvalstatustemp;
		}
		
		System.out.println("com.cisco.main.ReviewExecutor class :: propOverallEnvstatus ::" + propOverallEnvstatus);
		System.out.println("com.cisco.main.ReviewExecutor class :: proxyvalidationstatus ::" + proxyvalidationstatus);
		System.out.println("com.cisco.main.ReviewExecutor class :: propOverallvalidationstatus ::" + propOverallvalidationstatus);
		ValidationResponse valResponse = new ValidationResponse();

		// Overall status estimation and response collating response data
		if (validationoverride.equals("false")) {
			System.out.println("com.cisco.main.ReviewExecutor class :: Inside validation non override section--------------------->");
			if (proxyvalidationstatus.status && propOverallvalidationstatus) {
				// env is not applicable
				valResponse.env = "NA";
				valResponse.proxy_status = "Success";
				valResponse.message = proxyvalidationstatus.getMessage();
				valResponse.prop_val_status = "Success";
				valResponse.prop_env_status = "NA";
				valResponse.status = "Success";
				eventContext.getMessage().setPayload(valResponse);
			} else {
				valResponse.env = "NA";
				if (proxyvalidationstatus.status)
					valResponse.proxy_status = "Success";
				else
					valResponse.proxy_status = "Failed";
				valResponse.message = proxyvalidationstatus.getMessage();
				// if (propEnvstatus) {
				valResponse.prop_env_status = "NA";
				if (propOverallvalidationstatus)
					valResponse.prop_val_status = "Success";
				else {
					if (!propvalidationlist.isEmpty()) {
						propValFailedEnv = ReviewExecutor
								.firstPropValFailedEnv(propvalidationlist);
						valResponse.prop_val_status = "Failed - Discrepancy in implementation port or proxy path in "
								+ propValFailedEnv + ".properties";
					} else {
						valResponse.prop_val_status = "Failed - Discrepancy in implementation port or proxy path in config.properties";
					}
				}
				// } else {
				// valResponse.prop_env_status =
				// "Failed - config protperty file for given environment missing";
				// valResponse.prop_val_status =
				// "Failed - Property file not availbale for validation";
				// }

				valResponse.status = "Failure";
				eventContext.getMessage().setPayload(valResponse);
			}
		} else {
			System.out.println("com.cisco.main.ReviewExecutor class :: Inside validation override section--------------------->");
			if (propOverallvalidationstatus) {
				valResponse.env = "NA";
				valResponse.proxy_status = "NA";
				valResponse.message = "NA - Validation overrided";
				valResponse.prop_val_status = "Success";
				valResponse.prop_env_status = "NA";
				valResponse.status = "Success";
				eventContext.getMessage().setPayload(valResponse);
			} else {
				valResponse.env = "NA";
			    valResponse.proxy_status = "NA";
				valResponse.message = "NA - Validation overrided";
				// if (propEnvstatus) {
				valResponse.prop_env_status = "NA";
		//		if (propOverallvalidationstatus)
		//			valResponse.prop_val_status = "Success";
		//		else {
					if (!propvalidationlist.isEmpty()) {
						propValFailedEnv = ReviewExecutor
								.firstPropValFailedEnv(propvalidationlist);
						valResponse.prop_val_status = "Failed - Discrepancy in implementation port or proxy path in "
								+ propValFailedEnv + ".properties";
					} else {
						valResponse.prop_val_status = "Failed - Discrepancy in implementation port or proxy path in config.properties";
					}
		//		}
				// } else {
				// valResponse.prop_env_status =
				// "Failed - config protperty file for given environment missing";
				// valResponse.prop_val_status =
				// "Failed - Property file not availbale for validation";
				// }

				valResponse.status = "Failure";
				eventContext.getMessage().setPayload(valResponse);

			}
		}

		return eventContext.getMessage().getPayload();
	}
	
	public static String firstPropValFailedEnv(HashMap<String,Boolean> propValFailureList){
		for (Entry<String,Boolean> e : propValFailureList.entrySet()){
	        if (e.getValue().equals(false))
	        	return e.getKey();
		}
		return null;
	}

}
