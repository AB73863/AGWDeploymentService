/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.properties.CredentialDecoder;

public class DeleteDeploymentExceutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		String id = arg0.getMessage().getProperty("deploymentId",
				PropertyScope.INVOCATION);
		String mmchost = arg0.getMessage().getProperty("mmchost",
				PropertyScope.SESSION);
		String mmcport = arg0.getMessage().getProperty("mmcport",
				PropertyScope.SESSION);
		String mmcauth = arg0.getMessage().getProperty("mmcauth",
				PropertyScope.SESSION);
		CredentialDecoder decoder = new CredentialDecoder();
		String cred = decoder.getCredential(mmcauth);
		String mmcpath = arg0.getMessage().getMuleContext().getRegistry().get("deleteDeployment");
		System.out.println("com.cisco.main.DeleteDeploymentExecutor class :: Delete deployment mmcpath :"+ mmcpath);
		System.out.println("com.cisco.main.DeleteDeploymentExecutor class :: deployment id : " + id);
		try {

			// Upload the binary to the repository - Need file location ,
			// application name and version
			String[] commands = new String[] { "curl", "--basic", "-u",
					cred, "-X", "DELETE", "-k",
					"https://" + mmchost + ":"+mmcport+mmcpath+"/" + id };
			// String[] commands = new
			// String[]{"curl","--basic","-u","admin:admin","-X","DELETE","http://localhost:8585/mmc-3.7.0/api/repository/local$9fa6b6b7-3bb2-4f31-a52b-e0b6516b019d"};
			for (int i = 0; i < commands.length; i++) {
				System.out.println(commands[i]);
			}
			Process child = Runtime.getRuntime().exec(commands);
			InputStream stdin = child.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdin);
			BufferedReader br = new BufferedReader(isr);

			String line = null;
			System.out.println("<OUTPUT>");
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				sb.append(line);
			}

		} catch (IOException e) {
			System.out.println("com.cisco.main.DeleteDeploymentExecutor class :: IOException on call curl");
			e.printStackTrace();
		}

		return arg0.getMessage();
	}

}
