/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;
import org.mule.util.StringUtils;

import com.cisco.properties.CredentialDecoder;

public class MMCHostNameLookupExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String clusternameFromCsv = arg0.getMessage().getProperty(
				"clustername", PropertyScope.SESSION);
		Map<String,String> mmchostlist = arg0.getMessage().getProperty("mmchostnamelist",
				PropertyScope.SESSION);
		Map<String,String> mmcportlist = arg0.getMessage().getProperty("mmcportlist",
				PropertyScope.SESSION);
		Map<String,String> mmcauthlist = arg0.getMessage().getProperty("mmcauthlist",
				PropertyScope.SESSION);
		
		for(Map.Entry<String,String> entry : mmchostlist.entrySet()){
            System.out.printf("DC : %s and host: %s %n", entry.getKey(),
                                                           entry.getValue());
        }
		for(Map.Entry<String,String> entry : mmchostlist.entrySet()){
            System.out.printf("DC : %s and port: %s %n", entry.getKey(),
                                                           entry.getValue());
        }
		for(Map.Entry<String,String> entry : mmcauthlist.entrySet()){
//            System.out.printf("DC : %s and port: %s %n", entry.getKey(),
//                                                           entry.getValue());
        }
		String dc = StringUtils.substringBefore(clusternameFromCsv, "-"); 
		String hostname = mmchostlist.get(dc+"_mmchost");
		String port = mmcportlist.get(dc+"_mmcport");
		String auth = mmcauthlist.get(dc+"_mmcauth");
		if(hostname!=null)
			arg0.getMessage().setInvocationProperty("mmchost",hostname);
		else
			System.out.println("com.cisco.main.MMCHostNameLookupExecutor class :: Specified DC not found");
		System.out.println("com.cisco.main.MMCHostNameLookupExecutor class :: clsuernamefromcsv : "
				+ clusternameFromCsv +" DC:: "+dc+ " hostname:: " + hostname);
		
		if(port!=null)
			arg0.getMessage().setInvocationProperty("mmcport",port);
		else
			System.out.println("com.cisco.main.MMCHostNameLookupExecutor class :: Specified DC not found");
		System.out.println("com.cisco.main.MMCHostNameLookupExecutor class :: clsuernamefromcsv : "
				+ clusternameFromCsv +" DC:: "+dc+ " port:: " + port);
		
		if(auth!=null)
			arg0.getMessage().setInvocationProperty("mmcauth",auth);
		else
			System.out.println("com.cisco.main.MMCHostNameLookupExecutor class :: Specified DC not found");
		System.out.println("com.cisco.main.MMCHostNameLookupExecutor class :: clsuernamefromcsv : "
				+ clusternameFromCsv +" DC:: "+dc+ " auth:: " + auth);

		return arg0.getMessage();
	}
	

}
