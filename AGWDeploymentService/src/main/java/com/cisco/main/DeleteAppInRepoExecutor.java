/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.properties.CredentialDecoder;

public class DeleteAppInRepoExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		String appversionid = arg0.getMessage().getProperty("appversionid",
				PropertyScope.INVOCATION);
		String mmchost = arg0.getMessage().getProperty("mmchost",
				PropertyScope.SESSION);
		String mmcport = arg0.getMessage().getProperty("mmcport",
				PropertyScope.SESSION);
		String mmcauth = arg0.getMessage().getProperty("mmcauth",
				PropertyScope.SESSION);
		String mmcpath = arg0.getMessage().getMuleContext().getRegistry().get("deleteAppFromRepo");
		System.out.println("com.cisco.main.DeleteAppInRepoExecutor class :: Delete deployment mmcpath :"+ mmcpath);
		CredentialDecoder decoder = new CredentialDecoder();
		String cred = decoder.getCredential(mmcauth);
		System.out.println("com.cisco.main.DeleteAppInRepoExecutor class :: appversion id : " + appversionid);

		try {

			// Upload the binary to the repository - Need file location ,
			// application name and version
			String[] commands = new String[] {
					"curl",
					"--basic",
					"-u",
					cred,
					"-X",
					"DELETE",
					"-k",
					"https://" + mmchost + ":"+mmcport+mmcpath+"/"
							+ appversionid };
			for (int i = 0; i < commands.length; i++) {
				System.out.println(commands[i]);
			}
			Process child = Runtime.getRuntime().exec(commands);
			InputStream stdin = child.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdin);
			BufferedReader br = new BufferedReader(isr);

			String line = null;
			System.out.println("<OUTPUT>");
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				sb.append(line);
			}

		} catch (IOException e) {
			System.out.println("com.cisco.main.DeleteAppInRepoExecutor class :: IOException on curl call");
			e.printStackTrace();
		}

		return arg0.getMessage();
	}

}
