/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;
/**
 * @author arubalac
 */
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class ConfigPropNameExtractExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		String lifecycle = arg0.getMessage().getOutboundProperty("Lifecycle");
		String domain = arg0.getMessage().getOutboundProperty("Domain");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String configFileLocation = arg0.getMessage().getProperty(
				"ConfigFileLocation", PropertyScope.INVOCATION);
		String configFileName = domain + "-" + lifecycle.toLowerCase()
				+ ".config.properties";
		String constconfigFileLoc = drive + configFileLocation + domain + "/";
		System.out.println("com.cisco.main.ConfigPropNameExtractExecutor class :: configFileName : " + configFileName);
		System.out.println("com.cisco.main.ConfigPropNameExtractExecutor class :: constconfigFileLoc :" + constconfigFileLoc);
		arg0.getMessage().setProperty("Domain", domain, PropertyScope.OUTBOUND);
		arg0.getMessage().setProperty("ConfigFileName", configFileName,
				PropertyScope.OUTBOUND);
		arg0.getMessage().setProperty("ConfigFileLocation", constconfigFileLoc,
				PropertyScope.OUTBOUND);
		arg0.getMessage().setProperty("ExecutionGroupName", domain,
				PropertyScope.OUTBOUND);
		return arg0.getMessage();
	}

}
