/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.proxy.ProxyTypeSelector;

public class ProxyUpdateExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		// TODO Auto-generated method stub
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String filename = arg0.getMessage().getOutboundProperty("myFilename");
		String executionGroup = arg0.getMessage().getOutboundProperty(
				"ExecutionGroupName");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String repackoverride = arg0.getMessage().getOutboundProperty("repackageoverride");
		if(repackoverride.equalsIgnoreCase("false")){
		ProxyTypeSelector select = new ProxyTypeSelector();
		select.proxyselector(drive, baseUrl, filename, executionGroup);
		}
		return arg0.getMessage();
	}

}
