/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class ProxyUrlExtractExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		String domain = arg0.getMessage().getOutboundProperty("Domain");
		System.out.println("com.cisco.main.ProxyUrlExtractExecutor class :: URL-->" + domain);
		String[] afterSplit = domain.split("-");
		String token1 = afterSplit[0];
		String token2 = afterSplit[1];
		System.out.println("com.cisco.main.ProxyUrlExtractExecutor class :: token1-->" + token1 + " token2-->" + token2);
		String url = "/" + token1 + "/" + token2;
		System.out.println("com.cisco.main.ProxyUrlExtractExecutor class : Url--->" + url);
		arg0.getMessage().setProperty("ProxybaseUrl", url,
				PropertyScope.OUTBOUND);
		return arg0.getMessage();
	}

}
