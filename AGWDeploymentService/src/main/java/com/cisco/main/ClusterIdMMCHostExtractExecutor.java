/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import org.apache.commons.lang.StringUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class ClusterIdMMCHostExtractExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {

		String clustername = arg0.getMessage().getOutboundProperty(
				"tempclustername");
		String clusterid = arg0.getMessage().getOutboundProperty(
				"tempclusterid");
		String clusternameFromCsv = arg0.getMessage().getProperty(
				"clustername", PropertyScope.SESSION);
		System.out.println("com.cisco.main.ClusterIdMMCHostExtractExecutor class :: clustername ::"
				+ clustername + "  clsuernamefromcsv::" + clusternameFromCsv
				+ "  clusterid::" + clusterid);
		String fnlclstrnmedc1 = StringUtils.substringAfter(clusternameFromCsv, "-");
		if (clustername.equalsIgnoreCase(fnlclstrnmedc1))
			arg0.getMessage().setInvocationProperty("clusterid", clusterid);
		else
			System.out.println("com.cisco.main.ClusterIdMMCHostExtractExecutor class :: Provided clustername not matching any available cluster for getting cluster id");

		return arg0.getMessage();
	}
	

}
