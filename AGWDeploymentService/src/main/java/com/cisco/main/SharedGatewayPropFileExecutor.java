/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.properties.MultitenancyPropertyGen;

public class SharedGatewayPropFileExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
        
		ArrayList<String> envlist = new ArrayList<String>();
		envlist.add("dev");
		envlist.add("lt");
		envlist.add("stage");
		envlist.add("prod");
		
		String baseUrl = arg0.getMessage().getOutboundProperty(
				"baseUrl");
		String filename = arg0.getMessage().getOutboundProperty(
				"myFilename");
//		String env = arg0.getMessage().getOutboundProperty(
//				"Lifecycle");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String domain = arg0.getMessage().getOutboundProperty(
				"Domain");
//		String configFileName = arg0.getMessage().getOutboundProperty(
//				"ConfigFileName");
		String configFileName;
//		String configFileLocation = arg0.getMessage().getOutboundProperty(
//				"ConfigFileLocation");
		String configFileLocation = arg0.getMessage().getProperty("ConfigFileLocation",
				PropertyScope.INVOCATION);
		String destconfigDirectory  =  drive + baseUrl + filename+ "/classes/";

		for (String temp : envlist) {
		    configFileName =  domain+"-"+temp+".config.properties";
		    System.out.println("com.cisco.main.SharedGatewayPropFileExecutor class :: configfileanme "+configFileName+"configFileLocation "+drive+configFileLocation+domain+"/");
		    File configfile = new File(drive+configFileLocation+domain+"/"+configFileName);
		    if(temp.equals("dev") && !configfile.exists())
		    {
		    	System.out.println("com.cisco.main.SharedGatewayPropFileExecutor class :: Dev config file not found!!");
		    	throw new RuntimeException("Shared config file deosn't exist");
		    }else if(configfile.exists()){
		    	System.out.println("com.cisco.main.SharedGatewayPropFileExecutor class ::"+temp+".config file found!!");
			MultitenancyPropertyGen estorepropread = new MultitenancyPropertyGen();
			try {
				estorepropread.extractConfig(configFileName, drive+configFileLocation+domain+"/", destconfigDirectory,temp);
			} catch (IOException e) {
				System.out.println("com.cisco.main.SharedGatewayPropFileExecutor class :: IOException on reading the subscription property file");
				e.printStackTrace();
			}
		    }
		    
		}
		
		
		
		return arg0.getMessage();
	}

}
