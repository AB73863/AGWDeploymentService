/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.File;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.cleanup.TearDown;

public class TearDownExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		System.out.println("com.cisco.main.TearDownExecutor class :: Inside the teardown");
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String myFilename = arg0.getMessage().getOutboundProperty("myFilename");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		File folder = new File(drive + baseUrl + myFilename);
		TearDown teardown = new TearDown();
		teardown.deleteFolder(folder);
		return arg0.getMessage();
	}

}
