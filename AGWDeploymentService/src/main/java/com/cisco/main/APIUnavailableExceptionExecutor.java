package com.cisco.main;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.cisco.exception.APIUnavailableException;

public class APIUnavailableExceptionExecutor implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		throw new APIUnavailableException();
	}

}
