/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.io.File;

import org.apache.commons.io.FileUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class DeleteZipExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String filename = arg0.getMessage().getOutboundProperty("myFilename");
		String drive = arg0.getMessage().getOutboundProperty("Drive");
		File obj = new File(drive + baseUrl + filename + ".zip");
		FileUtils.deleteQuietly(obj);
		System.out.println("com.cisco.main.DeleteZipExecutor class :: Deletion completed!!!");
		return arg0.getMessage();
	}

}
