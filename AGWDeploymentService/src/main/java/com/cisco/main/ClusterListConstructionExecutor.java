/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.util.List;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.cisco.properties.DCClusterNameExtractor;

public class ClusterListConstructionExecutor implements Callable{

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String configFileName = arg0.getMessage().getOutboundProperty(
				"ConfigFileName");
		String configFileLocation = arg0.getMessage().getOutboundProperty(
				"ConfigFileLocation");	
		DCClusterNameExtractor clusternameext = new DCClusterNameExtractor();
		List<String> clusterList = clusternameext.getDcAndClusterName(
				configFileName, configFileLocation);
		arg0.getMessage().setPayload(clusterList);
		return arg0.getMessage();
	}

}
