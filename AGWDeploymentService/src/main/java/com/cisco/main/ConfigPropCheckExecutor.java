/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;
/**
 * @author arubalac
 */
import java.io.File;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.cisco.exception.ConfigFileCheckException;
import com.cisco.exception.SharedGWFileCheckException;

public class ConfigPropCheckExecutor implements Callable{

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		String baseUrl = arg0.getMessage().getOutboundProperty("baseUrl");
		String filename = arg0.getMessage().getOutboundProperty("myFilename");
		String env = arg0.getMessage().getOutboundProperty("Lifecycle");
		String drive = arg0.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String destsubscribeDirectory = drive + baseUrl + filename
				+ "/classes/" + "subscription." + env.toLowerCase()
				+ ".properties";
		String destconfigDirectory = drive + baseUrl + filename
				+ "/classes/" + "config." + env.toLowerCase()
				+ ".properties";
		System.out.println("com.cisco.main.ConfigPropCheckExecutor class :: SharedGWCheck : destsubscribeDirectory--->" + destsubscribeDirectory);
		System.out.println("com.cisco.main.ConfigPropCheckExecutor class :: SharedGWCheck :: destconfigDirectory--->" + destconfigDirectory);
		File sharedgwfile =  new File(destsubscribeDirectory);
		File configfile =  new File(destconfigDirectory);
		if(!configfile.exists())
			throw new ConfigFileCheckException("com.cisco.main.ConfigPropCheckExecutor class :: config file for "+env+" not found");
		if(!sharedgwfile.exists())
			throw new SharedGWFileCheckException("com.cisco.main.ConfigPropCheckExecutor class :: subscription config file for "+env+" not found");
		return arg0.getMessage();
	}

}
