/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.main;

/**
 * @author arubalac
 */
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class AppIdLookupExecutor implements Callable {

	@Override
	public Object onCall(MuleEventContext arg0) throws Exception {
		System.out.println("com.cisco.main.AppIdLookupExecutor class :: "+arg0.getMessage().getOutboundProperty(
				"PackageFolderLocation"));
		String PackageFolderLocation = arg0.getMessage().getOutboundProperty(
				"PackageFolderLocation");
		String baseUrl = FilenameUtils.getPath(PackageFolderLocation);
		String myFilename = FilenameUtils.getBaseName(PackageFolderLocation);
		System.out.println("com.cisco.main.AppIdLookupExecutor class ::" + "deployzip file : "+myFilename);
		Map<String, Object> map = new HashMap<>();
		map.put("baseUrl", baseUrl);
		// map.put("myFilename", myFilename);
		map.put("myFilename", myFilename.replace("-v", "-"));
		arg0.getMessage().addProperties(map, PropertyScope.OUTBOUND);
		return arg0.getMessage();
	}

}
