/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.validate;

/**
 * @author arubalac
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;

import com.cisco.bean.SrvlResponse;
import com.cisco.schval.check.App;
import com.cisco.schval.check.FileBean;
import com.cisco.schval.check.Response;

public class ProxyValidator {

	/**
	 * 
	 * @param schFileLoc
	 * @param xmlFileLoc
	 * @param outFileLoc
	 * @param env
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws TransformerException
	 */
	public SrvlResponse review(String schFileLoc, String xmlFileLoc,
			String outFileLoc) throws IOException,
			InterruptedException, TransformerException {
		SrvlResponse srvlresponse = new SrvlResponse();
		System.out
				.println("================================APG Codevalidator=============================");
		FileBean fileBean = new FileBean();
		App app = new App();
		File schFile = new File(schFileLoc);
		File xmlFile = new File(xmlFileLoc);
		fileBean.setXmlFile(xmlFile);
		fileBean.setSchFile(schFile);
		Response response = app.svrlReportGenerator(fileBean);
		System.out.println(response.getReport());

		// if (response.getReport().contains("failed-assert")
		// && (response.getReport().contains("MCOM") || response
		// .getReport().contains("MCON"))
		// && (env.equalsIgnoreCase("dev")
		// || env.equalsIgnoreCase("stage") || env
		// .equalsIgnoreCase("lt"))) {
		// System.out.println("java component inside");
		// srvlresponse.status = true;
		// srvlresponse.message =
		// "Warning - InfoSec disallowed processors used";
		// } else if (response.getReport().contains("failed-assert")
		// && (response.getReport().contains("MCOM") || response
		// .getReport().contains("MCON"))
		// && (env.equalsIgnoreCase("prod"))) {
		// srvlresponse.status = false;
		// srvlresponse.message =
		// "Critical - InfoSec disallowed processors used";
		// } else {
		// srvlresponse.status = true;
		// srvlresponse.message = "Success";
		// }
		if (response.getReport().contains("failed-assert")) {
			if (response.getReport().contains("MCOM")
					|| response.getReport().contains("MCON")) {
				srvlresponse.status = false;
				srvlresponse.message = "Critical - InfoSec disallowed processors used";
			} else {
				if (response.getReport().contains("AUTH")) {
					srvlresponse.status = true;
					srvlresponse.message = "Warning - Lastmile security is not enabled";
				} else {
					srvlresponse.status = true;
					srvlresponse.message = "Success";
				}
			}
		} else {
			srvlresponse.status = true;
			srvlresponse.message = "Success";
		}

		if (response.getReport().contains("failed-assert")) {
			System.out
					.println("=======================================VALIDATION STATUS============================================");
			System.out
					.println("Failed : Did not validate against the setup rules . Check the output SVRL file for failure assertion");
			System.out
					.println("====================================================================================================");

			File outfile = new File(outFileLoc);
			String filepath = outfile.getAbsolutePath().substring(0,
					outfile.getAbsolutePath().lastIndexOf(File.separator));
			File dir = new File(filepath);
			FileUtils.forceMkdir(dir);
			// if file doesnt exists, then create it
			// if (!outfile.exists()) {
			// outfile.createNewFile();
			// }
			FileWriter fw = new FileWriter(outfile);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(response.getReport());
			bw.close();
		} else {
			System.out
					.println("================================VALIDATION STATUS=============================");
			System.out.println("Passed : The code is good for promotion");
			System.out
					.println("================================VALIDATION STATUS=============================");
		}

		return srvlresponse;
	}

}