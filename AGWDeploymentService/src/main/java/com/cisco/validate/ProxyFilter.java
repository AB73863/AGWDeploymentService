/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.validate;
/**
 * @author arubalac
 */
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cisco.exception.ProxyFilterException;

public class ProxyFilter {

/**
 * 	
 * @param drive
 * @param baseUrl
 * @param filename
 * @throws ParserConfigurationException
 * @throws SAXException
 * @throws IOException
 * @throws TransformerFactoryConfigurationError
 * @throws TransformerException
 * @throws ProxyFilterException
 */
	public static  void proxyUpdate(String drive, String baseUrl, String filename) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException, ProxyFilterException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		System.out.println("com.cisco.proxy.ProxyFilter class :: "+drive + baseUrl + filename + "/proxy.xml");
		Document doc = docBuilder.parse(drive + baseUrl + filename
				+ "/proxy.xml");
		
		NodeList nodes = doc.getElementsByTagName("mule");
		Element element = (Element) nodes.item(0);
		NodeList httplisList = element.getElementsByTagName("http:listener");
		if(httplisList.item(0)!=null)
		System.out.println("com.cisco.proxy.ProxyFilter class :: "+httplisList.item(0).getNodeName());
		else
		throw new ProxyFilterException("Old Proxy");
		
	}

}
