/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.exception;
/**
 * 
 * @author arubalac
 *
 */
public class ConfigFileCheckException extends Exception{
	
	private static final long serialVersionUID = 1997753363232807009L;

	public ConfigFileCheckException()
	{
	}

	public ConfigFileCheckException(String message)
	{
		super(message);
	}

	public ConfigFileCheckException(Throwable cause)
	{
		super(cause);
	}

	public ConfigFileCheckException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ConfigFileCheckException(String message, Throwable cause, 
                                       boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
