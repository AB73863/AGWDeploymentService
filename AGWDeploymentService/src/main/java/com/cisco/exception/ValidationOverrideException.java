package com.cisco.exception;

public class ValidationOverrideException extends Exception{
	
	private static final long serialVersionUID = 1997753363232807009L;

	public ValidationOverrideException()
	{
	}

	public ValidationOverrideException(String message)
	{
		super(message);
	}

	public ValidationOverrideException(Throwable cause)
	{
		super(cause);
	}

	public ValidationOverrideException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ValidationOverrideException(String message, Throwable cause, 
                                       boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
