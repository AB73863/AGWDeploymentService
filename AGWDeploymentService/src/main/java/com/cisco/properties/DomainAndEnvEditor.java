/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.properties;

/**
 * @author arubalac
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class DomainAndEnvEditor {
	/**
	 * 
	 * @param domain
	 * @param dest
	 * @throws IOException
	 */
	public void setDomain(String domain, String dest) throws IOException {

		FileInputStream in;
		try {
			in = new FileInputStream(dest);
			Properties props = new Properties();
			props.load(in);
			in.close();

			FileOutputStream out = new FileOutputStream(dest);
			props.setProperty("domain", "shared-domain");
			//Enable this if multi-domain model
			//props.setProperty("domain", domain);
			props.store(out, null);
			out.close();
		} catch (FileNotFoundException e) {
			System.out.println("com.cisco.properties.DomainAndEnvEditor class :: FileNotFound exception when setting domain for mule.deploy property");
			throw new RuntimeException("mule-deploy.properties file not found");
		}

	}
	
	/**
	 * @param dest
	 * @throws IOException
	 */
	public void removeMuleEnv(String dest) throws IOException {

		FileInputStream in;
		try {
			in = new FileInputStream(dest);
			Properties props = new Properties();
			props.load(in);
			in.close();

			FileOutputStream out = new FileOutputStream(dest);
			if(props.containsKey("mule.env")){
			props.remove("mule.env");
			props.store(out, null);
			}
			out.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("mule-app.properties file not found");
		}

	}

}
