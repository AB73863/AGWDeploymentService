package com.cisco.properties;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.apache.commons.lang.StringUtils;

public class CredentialDecoder {
	
	public String getCredential(String authorization){
		String value = null;
		String encoded =  StringUtils.substringAfter(authorization, "Basic ");
		System.out.println("com.cisco.properties.CredentailDecoder class :: "+encoded);
		byte[] decoded = Base64.getDecoder().decode(encoded);
		System.out.println("com.cisco.properties.CredentailDecoder class :: "+decoded);
		try {
			value = new String(decoded, "UTF-8");
			//System.out.println(value);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        
		String Authroization = "Basic YWRtaW46YWRtaW4=";
		String encoded =  StringUtils.substringAfter(Authroization, "Basic ");
		System.out.println(encoded);
		byte[] decoded = Base64.getDecoder().decode(encoded);
		System.out.println(decoded);
		try {
			String value = new String(decoded, "UTF-8");
			System.out.println(value);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
