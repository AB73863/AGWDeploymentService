package com.cisco.properties;

/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
/**
 * @author arubalac
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author arubalac
 */
public class DCClusterNameExtractor {
	/**
	 * 
	 * @param configfilename
	 * @param configFileLoc
	 * @return
	 * @throws IOException
	 */
	public List<String> getDcAndClusterName(String configfilename,
			String configFileLoc) throws IOException {

		List<String> clusterlist = null;
		InputStream inStream = null;
		String source = configFileLoc + configfilename;;
		System.out.println("com.cisco.properties.DCClusterNameExtractor class :: Source--->" + source);
		Properties sharedconfigprop = new Properties();
		try {
			inStream = new FileInputStream(source);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Shared config file not found :: "
					+ source);
		}
		sharedconfigprop.load(inStream);
		HashMap<String, String> clusters = new HashMap<>();
		for (String key : sharedconfigprop.stringPropertyNames()) {
			if (key.contains("_clustername")) {
				String value = sharedconfigprop.getProperty(key);
				clusters.put(key, value);
			}
		}
		int clustercount = clusters.size();
		int tempcount = clustercount;
		System.out.println("com.cisco.properties.DCClusterNameExtractor class :: clustercount ::" + clustercount);
		clusterlist = new ArrayList<String>();
		for (Map.Entry<String, String> entry : clusters.entrySet()) {
			System.out.printf("com.cisco.properties.DCClusterNameExtractor class :: Key : %s and Value: %s %n", entry.getKey(),
					entry.getValue());
			System.out.println("com.cisco.properties.DCClusterNameExtractor class :: tempcount--" + tempcount);
			if (tempcount > 0) {
				if (!sharedconfigprop.getProperty(
						"dc" + tempcount + "_clustername").equalsIgnoreCase(""))
					clusterlist.add("dc"
							+ tempcount
							+ "-"
							+ sharedconfigprop.getProperty("dc" + tempcount
									+ "_clustername"));
			}
			tempcount--;
		}
		System.out.println("com.cisco.properties.DCClusterNameExtractor class :: clusterlist--->" + clusterlist.size()
				+ clusterlist.get(0));

		return clusterlist;

	}
}
