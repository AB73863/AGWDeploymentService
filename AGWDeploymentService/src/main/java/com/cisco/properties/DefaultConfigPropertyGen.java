/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.properties;

/**
 * @author arubalac
 */
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DefaultConfigPropertyGen {
	/**
	 * 
	 * @param drive
	 * @param baseUrl
	 * @param filename
	 * @param env
	 */
	public void addDefaultConfigProp(String drive, String baseUrl,
			String filename) {

		File source = new File(
				(drive + baseUrl + filename + "/classes/config.properties"));
		File dest = new File(
				(drive + baseUrl + filename + "/classes/config.dev.properties"));
		try {
			if (!dest.exists()) {
				System.out.println("com.cisco.properties.DefaultConfigPropertyGen class :: Copying the config prop to confg.dev.prop");
				DefaultConfigPropertyGen.copyFile(source, dest);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void copyFile(File source, File dest) throws IOException {
		Files.copy(source.toPath(), dest.toPath());
	}

}
