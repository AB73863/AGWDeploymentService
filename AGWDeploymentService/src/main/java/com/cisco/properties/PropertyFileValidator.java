/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.properties;

/**
 * @author arubalac
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyFileValidator {

	/**
	 * 
	 * @param drive
	 * @param baseUrl
	 * @param filename
	 * @param proxybaseUrl
	 * @param env
	 * @return
	 * @throws IOException
	 */
	public boolean propValidate(String drive, String baseUrl, String filename,
			String proxybaseUrl, String env) throws IOException {
		System.out.println("com.cisco.properties.PropertyFileValidator class :: Inside the PropValidate");
		System.out.println("com.cisco.properties.PropertyFileValidator class :: "+baseUrl);
		System.out.println("com.cisco.properties.PropertyFileValidator class :: "+filename);
		System.out.println("com.cisco.properties.PropertyFileValidator class :: "+proxybaseUrl);

		boolean propvalstatus = false;
		Properties prop = new Properties();
		InputStream in;
		System.out.println("com.cisco.properties.PropertyFileValidator class :: config file location ::" + drive + baseUrl
				+ filename + "/classes/config." + env + ".properties");
		in = new FileInputStream(drive + baseUrl + filename
				+ "/classes/config." + env + ".properties");
		System.out.println("com.cisco.properties.PropertyFileValidator class :: instream--->" + in);
		try {
			// load the drive name from a property file
			prop.load(in);
			String proxypath = prop.getProperty("proxy.path");
			String impPort = prop.getProperty("implementation.port");
			System.out.println("com.cisco.properties.PropertyFileValidator class :: proxypath-->" + proxypath);
			System.out.println("com.cisco.properties.PropertyFileValidator class :: impPort-->" + impPort);

			String[] proxypathtoken = proxypath.split("/");
			System.out.println("com.cisco.properties.PropertyFileValidator class :: "+ Arrays.toString(proxypathtoken));
			String[] proxybaseUrltoken = proxybaseUrl.split("/");
			System.out.println("com.cisco.properties.PropertyFileValidator class :: "+Arrays.toString(proxybaseUrltoken));
			if(proxypath.equals("") || proxypath.equals(" ") || proxypath.isEmpty() || proxypath==null)	{
				System.out.println("com.cisco.properties.PropertyFileValidator class :: The proxypath is null");
				propvalstatus = true;
			}
				
			if(!proxypath.isEmpty()){
				System.out.println("com.cisco.properties.PropertyFileValidator class :: The proxypath is not null");
			if (((proxypathtoken[1].equals(proxybaseUrltoken[1])) && ((proxypathtoken[2]
					.equals(proxybaseUrltoken[2]))))
					&& (impPort.equalsIgnoreCase("80")
							|| impPort.equalsIgnoreCase("8080") || impPort
								.equalsIgnoreCase("443")))
				propvalstatus = true;
			}
			// if (proxypath.contains(proxybaseUrl)// changing from equals to
			// contains
			// && (impPort.equalsIgnoreCase("80")
			// || impPort.equalsIgnoreCase("8080") || impPort
			// .equalsIgnoreCase("4443"))) {
			// propvalstatus = true;
			// }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("com.cisco.properties.PropertyFileValidator class :: PropertyFileValidator failed");
			e.printStackTrace();
		} finally {
			System.out
					.println("com.cisco.properties.PropertyFileValidator class :: PropertyFileValidator :: Executing connection close");
			in.close();
		}
		return propvalstatus;

	}

	/**
	 * 
	 * @param drive
	 * @param baseUrl
	 * @param filename
	 * @param proxybaseUrl
	 * @param env
	 * @return
	 * @throws IOException
	 */
	public boolean propValidateifNoEnv(String drive, String baseUrl,
			String filename, String proxybaseUrl)
			throws IOException {
		System.out.println("com.cisco.properties.PropertyFileValidator class :: Inside the PropValidateIfNoEnv");
		System.out.println(baseUrl);
		System.out.println(filename);
		System.out.println(proxybaseUrl);

		boolean propvalstatus = false;
		Properties prop = new Properties();
		InputStream in;
		System.out.println("com.cisco.properties.PropertyFileValidator class :: config file location ::" + drive + baseUrl
				+ filename + "/classes/config.properties");
		in = new FileInputStream(drive + baseUrl + filename
				+ "/classes/config.properties");
		System.out.println("com.cisco.properties.PropertyFileValidator class :: instream--->" + in);
		try {
			// load the drive name from a property file
			prop.load(in);
			String proxypath = prop.getProperty("proxy.path");
			String impPort = prop.getProperty("implementation.port");
			System.out.println("com.cisco.properties.PropertyFileValidator class :: proxypath-->" + proxypath);
			System.out.println("com.cisco.properties.PropertyFileValidator class :: impPort-->" + impPort);
			String[] proxypathtoken = proxypath.split("/");
			System.out.println("com.cisco.properties.PropertyFileValidator class :: "+Arrays.toString(proxypathtoken));
			String[] proxybaseUrltoken = proxybaseUrl.split("/");
			System.out.println("com.cisco.properties.PropertyFileValidator class :: "+Arrays.toString(proxybaseUrltoken));
			if (((proxypathtoken[1].equals(proxybaseUrltoken[1])) && ((proxypathtoken[2]
					.equals(proxybaseUrltoken[2]))))
					&& (impPort.equalsIgnoreCase("80")
							|| impPort.equalsIgnoreCase("8080") || impPort
								.equalsIgnoreCase("4443")))
				propvalstatus = true;

			// if (proxypath.contains(proxybaseUrl)// changing from equals to
			// contains
			// && (impPort.equalsIgnoreCase("80")
			// || impPort.equalsIgnoreCase("8080") || impPort
			// .equalsIgnoreCase("4443"))) {
			// propvalstatus = true;
			// }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("com.cisco.properties.PropertyFileValidator class :: PropertyFileValidator failed");
			e.printStackTrace();
		} finally {
			System.out
					.println("com.cisco.properties.PropertyFileValidator class :: PropertyFileValidator :: Executing connection close");
			in.close();
		}
		return propvalstatus;

	}

	// /**
	// *
	// * @param drive
	// * @param baseUrl
	// * @param filename
	// * @param env
	// * @return
	// */
	// public boolean checkPropEnv(String drive, String baseUrl, String
	// filename){
	// //String env) {
	// ArrayList<String> envlist = new ArrayList<String>();
	// envlist.add("dev");
	// envlist.add("lt");
	// envlist.add("stage");
	// envlist.add("prod");
	// //boolean propEnvSatus = false;
	//
	// File directory = new File(drive + baseUrl + filename + "/classes/");
	// File[] listOfFiles = directory.listFiles();
	//
	// for (File f : listOfFiles) {
	// System.out.println("About to Compare: " + filename + " : "
	// + f.getName());
	// if (f.getName().equalsIgnoreCase("config." + env + ".properties")) {
	// System.out.println("Found one! = " + filename);
	// propEnvSatus = true;
	// } else {
	// System.out.println("Not found matching property file!!");
	// }
	// }
	//
	// return propEnvSatus;
	// }

	/**
	 * 
	 * @param drive
	 * @param baseUrl
	 * @param filename
	 * @param env
	 * @return
	 */
	public Map<String, Boolean> checkPropEnv(String drive, String baseUrl, String filename) {
		// String env) {
		ArrayList<String> envlist = new ArrayList<String>();
		envlist.add("dev");
		envlist.add("lt");
		envlist.add("stage");
		envlist.add("prod");
		boolean propEnvSatus = false;
		HashMap<String, Boolean> envstatuslist = new HashMap<String, Boolean>();

		File directory = new File(drive + baseUrl + filename + "/classes/");
		File[] listOfFiles = directory.listFiles();

		for (String temp : envlist) {
			System.out.println("com.cisco.properties.PropertyFileValidator class :: Env : " + temp);
			for (File f : listOfFiles) {
				System.out.println("com.cisco.properties.PropertyFileValidator class :: About to Compare: " + filename + " : "
						+ f.getName());
				if (f.getName().equalsIgnoreCase(
						"config." + temp + ".properties")) {
					System.out.println("com.cisco.properties.PropertyFileValidator class :: Found one! = " + filename);
					propEnvSatus = true;
					envstatuslist.put(temp, propEnvSatus);
				} else {
					System.out.println("com.cisco.properties.PropertyFileValidator class :: Not found matching property file!!");
				}
			}
		}

		return envstatuslist;
	}

	public static final String PLAYER = "/devnet/test/";
	public static final String INPLAYER = "/devnet/test/auth";

	public static void main(String[] args) {
		String[] data = PLAYER.split("/");
		System.out.println(Arrays.toString(data));
		String[] data1 = INPLAYER.split("/");
		System.out.println(Arrays.toString(data1));
		if ((data[1].equals(data1[1])) && ((data[2].equals(data1[2]))))
			System.out.println("Success!!!");
		else
			System.out.println("Failed!!!");

	}

}
