/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.properties;

/**
 * @author arubalac
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

public class MultitenancyPropertyGen {

	int counter = 0;

	/**
	 * 
	 * @param configFileName
	 * @param configFileLocation
	 * @param destdir
	 * @param env
	 * @throws IOException
	 */
	public void extractConfig(String configFileName, String configFileLocation,
			String destdir, String env) throws IOException {

		String source = configFileLocation + configFileName;
		File destfile = new File(destdir + "subscription." + env.toLowerCase()
				+ ".properties");
		if (destfile.exists()) {
			return;
		}
		MultitenancyPropertyGen estorepropread = new MultitenancyPropertyGen();
		estorepropread.writeSharedGWPropFile(
				destdir + "subscription." + env.toLowerCase() + ".properties",
				source);

		Properties properties = new Properties();
		InputStream input = null;
		input = new FileInputStream(destdir + "subscription."
				+ env.toLowerCase() + ".properties");
		// load a properties file
		properties.load(input);

		for (String name : properties.stringPropertyNames()) {
			System.out.println(name);
			if (name.contains(".name")) {
				System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: prifix found--> " + name);
				counter++;
			}
		}
		System.out.println("counter ::" + counter);
		input.close();
		System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: Property file creation completed");
		FileInputStream instream = new FileInputStream(destdir
				+ "subscription." + env.toLowerCase() + ".properties");
		String content = IOUtils.toString(instream, "UTF-8");
		File crtfile = new File(destdir + "subscription." + env.toLowerCase()
				+ ".properties");
		FileOutputStream fileOut = new FileOutputStream(crtfile);
		try {
			for (int i = 1; i <= counter; i++) {
				// if(properties.getProperty("subscriptiongroup"+ i +
				// ".name")!=null){
				// try{
				String value = null;
				try {
					value = properties.getProperty("subscriptiongroup" + i
							+ ".name");
				} catch (Exception e) {
					System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: Exception in updating the subscription property file in proxy");
					e.printStackTrace();
				}
				if (value != null) {
					content = content.replace("subscriptiongroup" + i, value);
				}
				// }catch(Exception e){
				// System.out.println(e);
				// }
			}
			IOUtils.write(content, fileOut);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: Property file updated already");
		} finally {
			fileOut.close();

			instream.close();
		}
	}

	/**
	 * 
	 * @param destination
	 * @param requestSize
	 * @param responseSize
	 * @param responseTimeout
	 * @param configEnv
	 */
	public void PropFileAdder(String destination, String requestSize,
			String responseSize, String responseTimeout, String configEnv) {
		try {

			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: destination = " + destination);
			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: responseSize = " + responseSize);
			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: requestSize = " + requestSize);
			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: responseTimeout = " + responseTimeout);

			// Creating a new property file

			Properties crtproperties = new Properties();
			crtproperties.setProperty("responseSize", responseSize);
			crtproperties.setProperty("requestSize", requestSize);
			crtproperties.setProperty("responseTimeout", responseTimeout);

			File crtfile = new File(destination);
			FileOutputStream fileOut = new FileOutputStream(crtfile);
			crtproperties.store(fileOut, configEnv
					+ " api configuration details");
			fileOut.close();

			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: Property file creation completed");

		} catch (FileNotFoundException e) {
			System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: FileNotFound Exception when updating the scubscription property file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static int counter0 = 0;
	static int counter2 = 0;
	static int index = 1;

	/**
	 * 
	 * @param dest
	 * @param source
	 * @throws IOException
	 */
	public void writeSharedGWPropFile(String dest, String source)
			throws IOException {

		File file = new File(dest);
		BufferedReader fileReader = null;
		try {
			if (file.exists()) {
				fileReader = new BufferedReader(new FileReader(file));
				if (fileReader != null && !fileReader.readLine().isEmpty()) {
					return;
				}
			} else {
			}
		} finally {
			if (fileReader != null) {
				fileReader.close();
			}
		}

		System.out.println("com.cisco.properties.MultitenancyPropertyGen class :: estore config file loc ::" + source);
		File fin = new File(source);
		FileInputStream fis;
		try {

			fis = new FileInputStream(fin);
			BufferedReader in = new BufferedReader(new InputStreamReader(fis));
			FileWriter fstream;
			fstream = new FileWriter(dest, true);
			BufferedWriter out = new BufferedWriter(fstream);

			String aLine = null;
			while ((aLine = in.readLine()) != null) {
				// Process each line and add output to Dest.txt file
				out.write(aLine);
				out.newLine();
			}
			// do not forget to close the buffer reader
			in.close();

			// close buffer writer
			out.close();
		} catch (FileNotFoundException e) {
			System.out
					.println("com.cisco.properties.MultitenancyPropertyGen class :: eSoreConfigFIle not found on the specified directory :: "
							+ source);
			throw new RuntimeException(
					"com.cisco.properties.MultitenancyPropertyGen class :: eSoreConfigFIle not found on the specified directory :: "
							+ source);
		}
	}
}
