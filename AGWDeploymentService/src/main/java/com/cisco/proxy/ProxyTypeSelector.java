/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.proxy;

/**
 * @author arubalac
 */
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ProxyTypeSelector {
	/**
	 * 
	 * @param drive
	 * @param baseUrl
	 * @param filename
	 * @param executiongroup
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public void proxyselector(String drive, String baseUrl, String filename,
			String executiongroup) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		System.out.println("com.cisco.proxy.ProxyTypeSelector class :: "+drive + baseUrl + filename + "/proxy.xml");
		Document doc = docBuilder.parse(drive + baseUrl + filename
				+ "/proxy.xml");

		NodeList nodes = doc.getElementsByTagName("mule");
		Element element = (Element) nodes.item(0);
		NodeList cxfproxyservice = element
				.getElementsByTagName("cxf:proxy-service");
		NodeList apikitproxy = element.getElementsByTagName("apikit:proxy");
		if (cxfproxyservice.getLength() != 0) {
			System.out.println("com.cisco.proxy.ProxyTypeSelector class :: Wsdl based proxy");
			WSDLBasedProxyUpdater proxyupdate = new WSDLBasedProxyUpdater();
			proxyupdate.proxyUpdate(drive, baseUrl, filename, executiongroup);
		} else if (apikitproxy.getLength() != 0) {
			System.out.println("com.cisco.proxy.ProxyTypeSelector class :: RAML based proxy");
			RAMLBasedProxyUpdater proxyupdate = new RAMLBasedProxyUpdater();
			proxyupdate.proxyUpdate(drive, baseUrl, filename, executiongroup);
		} else {
			System.out.println("com.cisco.proxy.ProxyTypeSelector class :: HTTP based proxy");
			HTTPBasedProxyUpdater proxyupdate = new HTTPBasedProxyUpdater();
			proxyupdate.proxyUpdate(drive, baseUrl, filename, executiongroup);
		}
	}

}
