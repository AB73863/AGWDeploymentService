/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.proxy;

/**
 * @author arubalac
 */
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class HTTPBasedProxyUpdater {

	/**
	 * 
	 * @param drive
	 * @param baseUrl
	 * @param filename
	 * @param executiongroup
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public void proxyUpdate(String drive, String baseUrl, String filename,
			String executiongroup) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		System.out.println("com.cisco.proxy.HTTPBasedProxyUpdater class ::"+drive + baseUrl + filename + "/proxy.xml");
		Document doc = docBuilder.parse(drive + baseUrl + filename
				+ "/proxy.xml");

		// finding the root element and adding the scripting,spring,util
		// namespace and schemalocation
		Element originalDocumentElement = doc.getDocumentElement();
		originalDocumentElement.setAttribute("xmlns:scripting",
				"http://www.mulesoft.org/schema/mule/scripting");
		originalDocumentElement.setAttribute("xmlns:spring",
				"http://www.springframework.org/schema/beans");
		originalDocumentElement.setAttribute("xmlns:util",
				"http://www.springframework.org/schema/util");
		originalDocumentElement.setAttribute("xmlns:context",
				"http://www.springframework.org/schema/context");
		String schmloc = originalDocumentElement
				.getAttribute("xsi:schemaLocation");
		String schmlocfinal = schmloc
				.concat(" http://www.mulesoft.org/schema/mule/scripting http://www.mulesoft.org/schema/mule/scripting/current/mule-scripting.xsd http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-3.2.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-current.xsd");
		System.out.println("com.cisco.proxy.HTTPBasedProxyUpdater class ::" + schmlocfinal);
		originalDocumentElement
				.setAttribute("xsi:schemaLocation", schmlocfinal);

		NodeList nodes = doc.getElementsByTagName("mule");
		Element element = (Element) nodes.item(0);
		// finding the http:request-config tag to update the response time out
		// NodeList httpreqList = element
		// .getElementsByTagName("http:request-config");
		// System.out.println(httpreqList.item(0).getNodeName());
		// for (int i = 0; i < httpreqList.getLength(); i++) {
		// System.out.println("inside the loop--->");
		// Element httpreqElement = (Element) httpreqList.item(i);
		// System.out.println(httpreqElement);
		// httpreqElement
		// .setAttribute(
		// "responseTimeout",
		// "#[message.outboundProperties['responseTimeout']!=null ? Integer.parseInt(message.outboundProperties.responseTimeout) : 10000]");
		// }

		// Adding the spring util for property placeholder
		// String proputilholderxml =
		// "<spring:beans xmlns:spring=\"http://www.springframework.org/schema/beans\">\r\n <util:properties xmlns:util=\"http://www.springframework.org/schema/util\" id=\"configProperties\"\r\n location=\"subscription.${mule.env}.properties\"/>\r\n </spring:beans>\r\n ";
		// Document proputilscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(
		// proputilholderxml.getBytes()));
		// Node proputilscriptnode = doc.importNode(
		// proputilscriptdoc.getDocumentElement(), true);
		// element.appendChild(proputilscriptnode);

		// Adding the spring context for property placeholder
		// String propcontextholderxml =
		// "<context:property-placeholder xmlns:context=\"http://www.springframework.org/schema/context\" properties-ref=\"configProperties\" />";
		// Document propcntxtscriptdoc = docBuilder
		// .parse(new ByteArrayInputStream(propcontextholderxml.getBytes()));
		// Node propcntxtscriptnode = doc.importNode(
		// propcntxtscriptdoc.getDocumentElement(), true);
		// element.appendChild(propcntxtscriptnode);

		// finding the expression-language:property-placeholder tag to update
		// the prop filename
		// NodeList explList = element
		// .getElementsByTagName("expression-language:property-placeholder");
		// System.out.println(explList.item(0).getNodeName());
		// for (int i = 0; i < explList.getLength(); i++) {
		// System.out.println("inside the loop--->");
		// Element explElement = (Element) explList.item(i);
		// System.out.println(explElement);
		// explElement.setAttribute("location",
		// "config.${mule.env}.properties");
		// }

		// finding the http:listener tag to update the config-ref
//		NodeList httplisList = element.getElementsByTagName("http:listener");
//		System.out.println(httplisList.item(0).getNodeName());
//		for (int i = 0; i < httplisList.getLength(); i++) {
//			System.out.println("inside the loop--->");
//			Element httplisElement = (Element) httplisList.item(i);
//			System.out.println(httplisElement);
//			httplisElement.setAttribute("config-ref", "shared-http-listener-config");
//			//Enable this if multi-domain model
//			//httplisElement.setAttribute("config-ref", executiongroup);
//		}

		// finding the local HTTP listener and removing it
		NodeList httplistconfig = element
				.getElementsByTagName("http:listener-config");
		if(httplistconfig.item(0)!=null){
		System.out.println(httplistconfig.item(0).getNodeName());
		nodes.item(0).removeChild(httplistconfig.item(0));
		}

		// //finding the sublow tag and update the logging
		// String groovyscriptxml =
		// "<scripting:transformer xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" xmlns:scripting=\"http://www.mulesoft.org/schema/mule/scripting\" doc:name=\"Groovy\"> <scripting:script engine=\"Groovy\"><![CDATA[com.cisco.extension.LogMetadataTransformer logTransformerObject=new com.cisco.extension.LogMetadataTransformer(); logTransformerObject.transformMessage(message,null); return message;]]></scripting:script> </scripting:transformer>";
		// String loggerscriptxml =
		// "<logger xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" message=\"#[flowVars['metaDataLogger']]\" level=\"INFO\" doc:name=\"Logger\" category=\"com.cisco."+filename+"\"/>";
		// String removescriptxml =
		// "<remove-variable xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" variableName=\"metaDataLogger\" doc:name=\"Variable\"/>";
		// Document groovyscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(groovyscriptxml
		// .getBytes()));
		// Document loggerscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(loggerscriptxml
		// .getBytes()));
		// Document removescriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(removescriptxml
		// .getBytes()));
		// Node groovyscriptnode =
		// doc.importNode(groovyscriptdoc.getDocumentElement(), true);
		// Node loggerscriptnode =
		// doc.importNode(loggerscriptdoc.getDocumentElement(), true);
		// Node removescriptnode =
		// doc.importNode(removescriptdoc.getDocumentElement(), true);
		//
		// NodeList subflowList = element.getElementsByTagName("sub-flow");
		// System.out.println(subflowList.item(0).getNodeName());
		// if (subflowList.item(0).getNodeName().equalsIgnoreCase("sub-flow")
		// && subflowList.item(0).getAttributes().item(0).toString()
		// .equalsIgnoreCase("name=\"copy-headers\"")) {
		// NodeList subflwchldnodes = null;
		// Element subflowElement = null;
		// for (int i = 0; i < subflowList.getLength(); i++) {
		// System.out.println("inside the loop--->");
		// subflowElement = (Element) subflowList.item(i);
		// System.out.println(subflowElement);
		// // System.out.println(movieElement2.getChildNodes());
		// // movieElement2.appendChild(node);
		// subflwchldnodes = subflowElement.getChildNodes();
		// }
		// boolean isLoggerAvailable = false;
		// for (int j = 0; j < subflwchldnodes.getLength(); j++) {
		// System.out.println(subflwchldnodes.item(j).getNodeName());
		// if(subflwchldnodes.item(j).getNodeName().equalsIgnoreCase("scripting:transformer")){
		// Element groovyele = (Element) subflwchldnodes.item(j);
		// if
		// (subflwchldnodes.item(j).getNodeName().equalsIgnoreCase("scripting:transformer")
		// && groovyele.getAttribute("doc:name").toString()
		// .equalsIgnoreCase("Groovy")) {
		// isLoggerAvailable = true;
		//
		// }
		// }
		// }
		//
		// if (!isLoggerAvailable) {
		// System.out.println("choice node appended!!!");
		// subflowElement.appendChild(groovyscriptnode);
		// subflowElement.appendChild(loggerscriptnode);
		// subflowElement.appendChild(removescriptnode);
		// }
		// }
		//
		//
		//
		// // Adding the custom script for basic authorization , policy
		// validation
		// // framework
		// System.out.println("Adding the custom script----");
		// // String authrmvscriptxml =
		// //
		// "<remove-property xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" doc:name=\"Authorization\" propertyName=\"Authorization\" />";
		// // String authscriptxml =
		// //
		// "<scripting:transformer xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" xmlns:scripting=\"http://www.mulesoft.org/schema/mule/scripting\" doc:name=\"AuthroizationHeader\"><scripting:script engine=\"Groovy\"><![CDATA[if(flowVars['AuthorizationHeader']!=null){ message.setOutboundProperty('Authorization', flowVars['AuthorizationHeader']) } \n return message]]></scripting:script> </scripting:transformer>";
		// String msgpropscriptxml =
		// "<message-properties-transformer xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" doc:name=\"Message Properties\"> <add-message-property key=\"reqsize\" value=\"#[app.registry.configProperties[flowVars.key.concat('.requestsizelimit')]]\" /> <add-message-property key=\"respsize\" value=\"#[app.registry.configProperties[flowVars.key.concat('.responsesizelimit')]]\" /> <add-message-property key=\"resptimeout\" value=\"#[app.registry.configProperties[flowVars.key.concat('.responselatencylimit')]]\" /> <add-message-property key=\"tps\" value=\"#[app.registry.configProperties[flowVars.key.concat('.tps')]]\" /> </message-properties-transformer>";
		// String proxyvalscriptxml =
		// "<scripting:transformer xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" xmlns:scripting=\"http://www.mulesoft.org/schema/mule/scripting\" doc:name=\"PolicyValidaton\"> <scripting:script engine=\"Groovy\"><![CDATA[ com.cisco.apg.validation.PolicyValidator proxyvalmgr = new com.cisco.apg.validation.PolicyValidator(); proxyvalmgr.proxyval(message); \n return message]]></scripting:script> </scripting:transformer>";
		// String exceptionscriptxml =
		// "<choice-exception-strategy xmlns=\"http://www.mulesoft.org/schema/mule/core\" xmlns:doc=\"http://www.mulesoft.org/schema/mule/documentation\" doc:name=\"Choice Exception Strategy\"> <catch-exception-strategy when=\"#[exception.causedExactlyBy(java.util.concurrent.TimeoutException)]\" doc:name=\"TimeOutException\"> <logger message=\"Exception due to timeout\" level=\"INFO\" doc:name=\"Logger\" /> <set-property propertyName=\"http.status\" value=\"403\" doc:name=\"Property\" /> <set-payload value=\"Exception due to timeout\" doc:name=\"Set Payload\" /> </catch-exception-strategy> <catch-exception-strategy doc:name=\"RequestSize-PolicyValueViolation\" when=\"#[exception.cause.message.contains('MandatoryPolicyValueViolation-reqSize')]\"> <logger message=\"Policy not applied - #[exception.cause.message]\" level=\"INFO\" doc:name=\"Logger\" /> <set-property propertyName=\"http.status\" value=\"403\" doc:name=\"Property\" /> <set-payload value=\"RequestSize value set on messagesize policy not allowed\" doc:name=\"Set Payload\" /> </catch-exception-strategy> <catch-exception-strategy doc:name=\"ResponseSize-PolicyValueViolation\" when=\"#[exception.cause.message.contains('MandatoryPolicyValueViolation-resSize')]\"> <logger message=\"Policy not applied - #[exception.cause.message]\" level=\"INFO\" doc:name=\"Logger\" /> <set-property propertyName=\"http.status\" value=\"403\" doc:name=\"Property\" /> <set-payload value=\"ResponseSize value set on messagesize policy not allowed\" doc:name=\"Set Payload\" /> </catch-exception-strategy> <catch-exception-strategy doc:name=\"ResponseTime-PolicyValueViolation\" when=\"#[exception.cause.message.contains('MandatoryPolicyValueViolation-resTime')]\"> <logger message=\"Policy not applied - #[exception.cause.message]\" level=\"INFO\" doc:name=\"Logger\" /> <set-property propertyName=\"http.status\" value=\"403\" doc:name=\"Property\" /> <set-payload value=\"ResponseTime value set on responsetimeout policy not allowed\" doc:name=\"Set Payload\" /> </catch-exception-strategy> <catch-exception-strategy doc:name=\"ConnectionTime-PolicyValueViolation\" when=\"#[exception.cause.message.contains('MandatoryPolicyValueViolation-conTime')]\"> <logger message=\"Policy not applied - #[exception.cause.message]\" level=\"INFO\" doc:name=\"Logger\" /> <set-property propertyName=\"http.status\" value=\"403\" doc:name=\"Property\" /> <set-payload value=\"ConnectionTime value set on responsetimeout policy not allowed\" doc:name=\"Set Payload\" /> </catch-exception-strategy><catch-exception-strategy doc:name=\"Mandatory-PolicyViolation\" when=\"#[exception.cause.message.contains('MandatoryPolicyViolation')]\"><logger message=\"Policy not applied - #[exception.cause.message]\" level=\"INFO\" doc:name=\"Logger\" /><set-property propertyName=\"http.status\" value=\"403\" doc:name=\"Property\" /><set-payload value=\"Mandatory policies not applied\" doc:name=\"Set Payload\" /></catch-exception-strategy></choice-exception-strategy>";
		// // Document customscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(
		// // authrmvscriptxml.getBytes()));
		// // Document authscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(
		// // authscriptxml.getBytes()));
		// Document msgpropscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(
		// msgpropscriptxml.getBytes()));
		// Document proxyvalscriptdoc = docBuilder.parse(new
		// ByteArrayInputStream(
		// proxyvalscriptxml.getBytes()));
		// Document exceptionscriptdoc = docBuilder
		// .parse(new ByteArrayInputStream(exceptionscriptxml.getBytes()));
		// // Node authrmvscriptnode = doc.importNode(
		// // customscriptdoc.getDocumentElement(), true);
		// // Node authgscriptnode = doc.importNode(
		// // authscriptdoc.getDocumentElement(), true);
		// Node msgpropscriptnode = doc.importNode(
		// msgpropscriptdoc.getDocumentElement(), true);
		// Node proxyvalscriptnode = doc.importNode(
		// proxyvalscriptdoc.getDocumentElement(), true);
		// Node exceptionscriptnode = doc.importNode(
		// exceptionscriptdoc.getDocumentElement(), true);
		//
		// NodeList FlowList = element.getElementsByTagName("flow");
		// System.out.println(FlowList.item(0).getNodeName()
		// + FlowList.item(0).getAttributes().item(0));
		//
		// NodeList flowchildnodes = null;
		// Element flowElement = null;
		// boolean isAuthorization = false;
		// for (int i = 0; i < FlowList.getLength(); i++) {
		// System.out.println("inside the flow loop--->");
		// flowElement = (Element) FlowList.item(i);
		// System.out.println("movie element 6--->" + flowElement);
		// flowchildnodes = flowElement.getChildNodes();
		// for (int j = 0; j < flowchildnodes.getLength(); j++) {
		// System.out.println(flowchildnodes.item(j).getNodeName());
		// if (flowchildnodes.item(j).getNodeName()
		// .equalsIgnoreCase("remove-property")) {
		// Element choice = (Element) flowchildnodes.item(j);
		// if (flowchildnodes.item(j).getNodeName()
		// .equalsIgnoreCase("remove-property")
		// && choice.getAttribute("doc:name").toString()
		// .equalsIgnoreCase("Authorization")) {
		// isAuthorization = true;
		//
		// }
		// }
		// }
		// }
		//
		// if (!isAuthorization) {
		// NodeList flowList = doc.getElementsByTagName("flow");
		// for (int j = 0; j < flowList.getLength(); j++) {
		// Node flowNode = flowList.item(j);
		// NodeList flowChildNodes = flowNode.getChildNodes();
		// System.out.println("------------" + "FLOW" + j
		// + "----------------");
		// for (int i = 0; i < flowChildNodes.getLength(); i++) {
		// if (flowChildNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
		// Node childNode = flowChildNodes.item(i);
		// if (childNode.getNodeName().equalsIgnoreCase(
		// "http:listener")) {
		// int counter = 1;
		// do {
		// childNode = flowChildNodes.item(i + counter);
		// counter++;
		// } while (childNode.getNodeType() != Node.ELEMENT_NODE);
		// // The below snippet will add the remove property
		// // for authorization header
		// // childNode.getParentNode().insertBefore(
		// // authrmvscriptnode.cloneNode(true),
		// // childNode);
		// // The below snippet will add the authorization
		// // headersetting groovy script
		// // childNode.getParentNode().insertBefore(
		// // authgscriptnode.cloneNode(true), childNode);
		// // The below snippet will add the message property
		// // transformer
		// childNode.getParentNode().insertBefore(
		// msgpropscriptnode.cloneNode(true),
		// childNode);
		// // The below snippet will add the proxy validation
		// // groovy script
		// childNode.getParentNode().insertBefore(
		// proxyvalscriptnode.cloneNode(true),
		// childNode);
		// }
		// }
		//
		// }
		// flowNode.appendChild(exceptionscriptnode.cloneNode(true));
		// }
		// }
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		StreamResult result = new StreamResult(new File(drive + baseUrl
				+ filename + "/proxy.xml"));
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
	}

}
