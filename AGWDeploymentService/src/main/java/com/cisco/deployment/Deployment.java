/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.deployment;

/**
 * @author arubalac
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONObject;
import org.mule.api.transport.PropertyScope;

import com.cisco.properties.CredentialDecoder;

public class Deployment {
/**
 * 
 * @param fileLocation
 * @param appName
 * @param version
 * @param mmchost
 * @return
 * @throws InterruptedException
 */
	public String uploadAppToRepo(String fileLocation, String appName,
			String version, String mmchost,String mmcport,String mmcpath,String mmcauth) throws InterruptedException {
		String applicationverId = null;
		CredentialDecoder decoder = new CredentialDecoder();
		String cred = decoder.getCredential(mmcauth);
		try {

			// Upload the binary to the repository - Need file location ,
			// application name and version
			// String[] commands = new
			// String[]{"curl","--basic","-u","admin:admin","-F","file=@\\Users\\arubalac\\Downloads\\MMCTest-v1.0.0-1.3.x.zip","-F","name=MMCTest","-F","version=2.0","--header",
			// "\"Content-Type:", "multipart/form-data\"",
			// "http://localhost:8585/mmc-3.7.0/api/repository"};
			String[] commands = new String[] { "curl", "--basic", "-u",
					cred, "-F", "file=@" + fileLocation, "-F",
					"name=" + appName, "-F", "version=" + version, "--header",
					"\"Content-Type:", "multipart/form-data\"", "-k",
					"https://" + mmchost + ":"+mmcport+ mmcpath };
			for (int i = 0; i < commands.length; i++) {
				System.out.println("com.cisco.deployment.Deployment class ::"+ commands[i]);
			}
			Process child = Runtime.getRuntime().exec(commands);
			InputStream stdin = child.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdin);
			BufferedReader br = new BufferedReader(isr);

			String line = null;
			System.out.println("<OUTPUT>");
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				sb.append(line);
			}
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("com.cisco.deployment.Deployment class ::"+json.get("versionId"));
			applicationverId = json.getString("versionId");
			System.out.println("</OUTPUT>");
			int exitVal = child.waitFor();
			System.out.println("com.cisco.deployment.Deployment class :: Process exitValue: " + exitVal);

		} catch (IOException e) {
			System.out.println("com.cisco.deployment.Deployment class :: IOException from curl call");
			e.printStackTrace();
		}

		return applicationverId;
	}

}
