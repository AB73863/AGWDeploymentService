/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.cleanup;

/**
 * 
 * @author arubalac
 *
 */
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class TearDown {
/**
 * 
 * @param folder
 */
	public void deleteFolder(File folder) {
		try {
			FileUtils.deleteDirectory(folder);
		} catch (IOException e) {
			System.out.println("com.cisco.cleanup.TearDown class :: Teardown failed - Deletion of the folder fail");
			e.printStackTrace();
		}
	}

}
