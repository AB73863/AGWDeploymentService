/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.zip.manage;

/**
 * @author arubalac
 */
import java.io.File;
import java.io.IOException;

import org.codehaus.plexus.archiver.zip.ZipArchiver;

public class ZipUtility {

	/**
	 * 
	 * @param dirToZip
	 * @param zipFilepath
	 * @throws IOException
	 */
	public void zipFolder(String dirToZip, String zipFilepath)
			throws IOException {

		System.out.println("com.cisco.zip.manage.ZipUtility class :: "+ dirToZip);
		System.out.println("com.cisco.zip.manage.ZipUtility class :: "+zipFilepath);
		zipDir(dirToZip, zipFilepath);
	}

	/**
	 * 
	 * @param dirToZip
	 * @param zipFile
	 * @return
	 * @throws IOException
	 */
	public File zipDir(String dirToZip, String zipFile) throws IOException {
		ZipArchiver archiver = new ZipArchiver();
		archiver.addDirectory(new File(dirToZip));
		File zip = new File(zipFile);
		archiver.setDestFile(zip);
		archiver.createArchive();
		return zip;
	}

}
