/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.zip.manage;

/**
 * @author arubalac
 */
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class Unzip implements Callable {

	public Object onCall(MuleEventContext eventContext) throws Exception {
		System.out.println("com.cisco.zip.manage.Unzip class :: "+eventContext.getMessage().getOutboundProperty(
				"PackageFolderLocation"));
		String packageFolderLocation = eventContext.getMessage()
				.getOutboundProperty("PackageFolderLocation");
		String drive = eventContext.getMessage().getProperty("Drive",
				PropertyScope.INVOCATION);
		String baseUrl = FilenameUtils.getPath(packageFolderLocation);
		String myFilename = FilenameUtils.getBaseName(packageFolderLocation);

		Map<String, Object> map = new HashMap<>();
		map.put("baseUrl", baseUrl);
		map.put("myFilename", myFilename);
		eventContext.getMessage().addProperties(map, PropertyScope.OUTBOUND);

		System.out.println("com.cisco.zip.manage.Unzip class :: "+baseUrl);

		UnzipUtility obj = new UnzipUtility();
		try {
			System.out.println("com.cisco.zip.manage.Unzip class :: "+drive + baseUrl + myFilename);
			obj.unzip(packageFolderLocation, drive + baseUrl + myFilename);
		} catch (IOException e) {
			System.out.println("com.cisco.zip.manage.Unzip class :: Unzip Executor failed");
			e.printStackTrace();
		}

		return eventContext.getMessage();
	}

}
