/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.zip.manage;

/**
 * @author arubalac
 */
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipUtility {

	/**
	 * Size of the buffer to read/write data
	 */
	private static final int BUFFER_SIZE = 10240;

	/**
	 * Extracts a zip file specified by the zipFilePath to a directory specified
	 * by destDirectory (will be created if does not exists)
	 * 
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void unzip(String zipFilePath, String destDirectory)
			throws IOException, InterruptedException {
		System.out.println("com.cisco.zip.manage.UnzipUtility class :: zipFilePath-->" + zipFilePath + " and "
				+ "destDirectory--->" + destDirectory);
		ZipInputStream zipIn = new ZipInputStream(new FileInputStream(
				zipFilePath));
		try {
			File destDir = new File(destDirectory);
			if (!destDir.exists()) {
				System.out.println("com.cisco.zip.manage.UnzipUtility class :: create a directory");
				destDir.mkdir();
				Thread.sleep(1000);
			}
			ZipEntry entry = zipIn.getNextEntry();
			// iterates over entries in the zip file
			while (entry != null) {
				System.out.println("com.cisco.zip.manage.UnzipUtility class :: Files--->" + destDirectory + File.separator
						+ entry.getName());
				String filePath = destDirectory + File.separator
						+ entry.getName();
				if (!entry.isDirectory()) {
					// if the entry is a file, extracts it
					extractFile(zipIn, filePath);
				} else {
					// if the entry is a directory, make the directory
					File dir = new File(filePath);
					dir.mkdir();
				}

				entry = zipIn.getNextEntry();
			}
		} catch (Exception e) {
			System.out.println("com.cisco.zip.manage.UnzipUtility class :: Unzip utility failed");
		} finally {
			System.out.println("com.cisco.zip.manage.UnzipUtility class ::  Executing connection close");
			zipIn.closeEntry();
			zipIn.close();
		}
	}

	/**
	 * Extracts a zip entry (file entry)
	 * 
	 * @param zipIn
	 * @param filePath
	 * @throws IOException
	 */
	private void extractFile(ZipInputStream zipIn, String filePath)
			throws IOException {
		BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(filePath));
		byte[] bytesIn = new byte[BUFFER_SIZE];
		int read = 0;
		while ((read = zipIn.read(bytesIn)) != -1) {
			bos.write(bytesIn, 0, read);
		}
		bos.close();
	}

}
