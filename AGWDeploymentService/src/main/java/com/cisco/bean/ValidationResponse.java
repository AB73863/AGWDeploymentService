/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.bean;

/**
 * 
 * @author arubalac
 *
 */
public class ValidationResponse {

	public String message = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String isStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String isProxy_status() {
		return proxy_status;
	}

	public void setProxy_status(String proxy_status) {
		this.proxy_status = proxy_status;
	}

	public String isProp_env_status() {
		return prop_env_status;
	}

	public void setProp_env_status(String prop_env_status) {
		this.prop_env_status = prop_env_status;
	}

	public String isProp_val_status() {
		return prop_val_status;
	}

	public void setProp_val_status(String prop_val_status) {
		this.prop_val_status = prop_val_status;
	}

	public String status = null;
	public String env = null;
	public String proxy_status = null;
	public String prop_env_status = null;
	public String prop_val_status = null;

}
