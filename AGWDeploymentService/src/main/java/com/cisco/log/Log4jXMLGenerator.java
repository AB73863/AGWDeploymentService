/*
 * (c) 2003-2015 cisco systems, Inc. This software is protected under international copyright
 * law. All use of this software is subject to CISCO Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * cisco. If such an agreement is not in place, you may not use the software.
 */
package com.cisco.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;



public class Log4jXMLGenerator {
	

	private String log4jxmlpart1 = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Configuration monitorInterval=\"25\"><Appenders><RollingFile name=\"";
	private String log4jxmlpart2 = "file\" fileName=\"${sys:mule.home}${sys:file.separator}logs";
	private String log4jxmlpart3 = "${sys:file.separator}";
	private String log4jxmlpart4 = ".log\" filePattern=\"${sys:mule.home}${sys:file.separator}logs${sys:file.separator}";
	private String log4jxmlpart5 = "-%i.log\"><PatternLayout pattern=\"%d [%t] %-5p %c - %m%n\" /> <SizeBasedTriggeringPolicy size=\"10 MB\" /><DefaultRolloverStrategy max=\"10\"/></RollingFile></Appenders><Loggers><!-- CXF is used heavily by Mule for web services --><AsyncLogger name=\"org.apache.cxf\" level=\"WARN\"/><!-- Apache Commons tend to make a lot of noise which can clutter the log--> <AsyncLogger name=\"org.apache\" level=\"WARN\"/>  <!-- Reduce startup noise --> <AsyncLogger name=\"org.springframework.beans.factory\" level=\"WARN\"/>  <!-- Mule classes --> <AsyncLogger name=\"org.mule\" level=\"INFO\"/> <AsyncLogger name=\"com.mulesoft\" level=\"INFO\"/>  <!-- Reduce DM verbosity --> <AsyncLogger name=\"org.jetel\" level=\"WARN\"/> <AsyncLogger name=\"Tracking\" level=\"WARN\"/>";
//	private String log4jxmlpart6 = "<AppenderRef ref=\"";
	private String log4jxmlpart7 = "<AsyncRoot level=\"INFO\"> <AppenderRef ref=\"file\" /> </AsyncRoot> </Loggers> </Configuration>";
	
	public void addLog4j(String destdir , String filename , String domain){
		System.out.println("com.cisco.log.Log4jXMLGenerator class ::"+ "destdirectory" +destdir + "filename" + filename + "domain" + domain );
	       StringBuilder sBuffer = new StringBuilder();
	       sBuffer.append(log4jxmlpart1);
	       sBuffer.append(log4jxmlpart2);
	       sBuffer.append(log4jxmlpart3);
	       sBuffer.append(domain);
	       sBuffer.append(log4jxmlpart3);
	       sBuffer.append("mule-app-"+filename);
	       sBuffer.append(log4jxmlpart4);
	       sBuffer.append("mule-app-"+filename);
	       sBuffer.append(log4jxmlpart5);
//	       sBuffer.append("logging");
//	       sBuffer.append(log4jxmlpart6);
//	       sBuffer.append("file");
	       sBuffer.append(log4jxmlpart7);
	       System.out.println(sBuffer); 
	       File file = new File(destdir+"log4j2.xml");
		    // if file doesnt exists, then create it
	       try {
		    			if (!file.exists()) {
		    				
								file.createNewFile();
							
		    			}
	
		    			FileWriter fw = new FileWriter(file.getAbsoluteFile());
		    			BufferedWriter bw = new BufferedWriter(fw);
		    			bw.write(sBuffer.toString());
		    			bw.close();
		    			
		    			//Deleting the existing log4j2-test.xml
		    			File log4jtestxml = new File(destdir+"log4j2-test.xml");
		        		if(log4jtestxml.delete()){
		        			System.out.println("com.cisco.log.Log4jXMLGenerator class ::"+log4jtestxml.getName() + " is deleted!");
		        		}else{
		        			System.out.println("com.cisco.log.Log4jXMLGenerator class :: log4j-test xml Delete operation is failed.");
		        		}
		    			
	       } catch (IOException e) {
				System.out.println("com.cisco.log.Log4jXMLGenerator class :: Log4jXML file writting failed!!");
				e.printStackTrace();
			}
		    			System.out.println("com.cisco.log.Log4jXMLGenerator class :: Done");
	}
	
//	public static void main(String args[]) throws IOException{
//	       StringBuilder sBuffer = new StringBuilder();
//	       sBuffer.append(log4jxmlpart1);
//	       sBuffer.append(appname);
//	       sBuffer.append(log4jxmlpart2);
//	       sBuffer.append(log4jxmlpart3);
//	       sBuffer.append(domain);
//	       sBuffer.append(log4jxmlpart3);
//	       sBuffer.append(appname);
//	       sBuffer.append(log4jxmlpart4);
//	       sBuffer.append(appname);
//	       sBuffer.append(log4jxmlpart5);
//	       sBuffer.append(appname);
//	       sBuffer.append(log4jxmlpart6);
//	       sBuffer.append(appname);
//	       sBuffer.append(log4jxmlpart7);
//	       System.out.println(sBuffer); 
//	       String content = sBuffer.toString();
//	       File file = new File("C:/CISCO/Testlog.xml");
//	    // if file doesnt exists, then create it
//	    			if (!file.exists()) {
//	    				file.createNewFile();
//	    			}
//
//	    			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//	    			BufferedWriter bw = new BufferedWriter(fw);
//	    			bw.write(content);
//	    			bw.close();
//
//	    			System.out.println("Done");
//	   }
  
}
