<schema xmlns="http://purl.oclc.org/dsdl/schematron">
  <title>CISCO-SDN_API</title>
  <ns prefix="doc" uri="http://www.mulesoft.org/schema/mule/documentation"/>
  <ns prefix="http" uri="http://www.mulesoft.org/schema/mule/http"/>
  <ns prefix="data-mapper" uri="http://www.mulesoft.org/schema/mule/ee/data-mapper"/>
  <ns prefix="mule" uri="http://www.mulesoft.org/schema/mule/core"/>
  <ns prefix="ajax" uri="http://www.mulesoft.org/schema/mule/ajax"/>
  <ns prefix="magento" uri="http://www.mulesoft.org/schema/mule/magento"/> 
  <ns prefix="facebook" uri="http://www.mulesoft.org/schema/mule/facebook"/> 
  <ns prefix="jetty" uri="http://www.mulesoft.org/schema/mule/jetty"/>
  <ns prefix="cmis" uri="http://www.mulesoft.org/schema/mule/cmis"/>
  <ns prefix="jms" uri="http://www.mulesoft.org/schema/mule/jms"/> 
  <ns prefix="s3" uri="http://www.mulesoft.org/schema/mule/s3"/> 
  <ns prefix="quartz" uri="http://www.mulesoft.org/schema/mule/quartz"/> 
  <ns prefix="sap" uri="http://www.mulesoft.org/schema/mule/sap"/> 
  <ns prefix="ftp" uri="http://www.mulesoft.org/schema/mule/ee/ftp"/> 
  <ns prefix="ajax" uri="http://www.mulesoft.org/schema/mule/ajax"/> 
  <ns prefix="pop3" uri="http://www.mulesoft.org/schema/mule/pop3"/> 
  <ns prefix="file" uri="http://www.mulesoft.org/schema/mule/file"/> 
  <ns prefix="db" uri="http://www.mulesoft.org/schema/mule/db"/> 
  <ns prefix="rmi" uri="http://www.mulesoft.org/schema/mule/rmi"/> 
  <ns prefix="sqs" uri="http://www.mulesoft.org/schema/mule/sqs"/>
  <ns prefix="mongo" uri="http://www.mulesoft.org/schema/mule/mongo"/> 
  <ns prefix="imap" uri="http://www.mulesoft.org/schema/mule/imap"/>
  <ns prefix="vm" uri="http://www.mulesoft.org/schema/mule/vm"/> 
  <ns prefix="twitter" uri="http://www.mulesoft.org/schema/mule/twitter"/>
  <ns prefix="servlet" uri="http://www.mulesoft.org/schema/mule/servlet"/>
  <ns prefix="ssl" uri="http://www.mulesoft.org/schema/mule/ssl"/>
  <ns prefix="wmq" uri="http://www.mulesoft.org/schema/mule/ee/wmq"/> 
  <ns prefix="smtp" uri="http://www.mulesoft.org/schema/mule/smtp"/> 
  <ns prefix="sfdc" uri="http://www.mulesoft.org/schema/mule/sfdc"/> 
  <ns prefix="tcp" uri="http://www.mulesoft.org/schema/mule/tcp"/> 
  <ns prefix="udp" uri="http://www.mulesoft.org/schema/mule/udp"/> 
  <ns prefix="sftp" uri="http://www.mulesoft.org/schema/mule/sftp"/> 
  <ns prefix="ws" uri="http://www.mulesoft.org/schema/mule/ws"/> 
  <ns prefix="scripting" uri="http://www.mulesoft.org/schema/mule/scripting"/>
	<pattern name="MulePattern">
	
	<!-- <rule context="mule:sub-flow[1]">
      <assert test="@name = 'security-validation'">AUTH::Authorization code not available</assert>
      <report test="@name = 'security-validation'">success</report>
	  <assert test="(scripting:transformer[@doc:name = 'CiscoBackendSecurity'])">AUTH::Authorization code not available</assert>
      <report test="(scripting:transformer[@doc:name = 'CiscoBackendSecurity'])">success</report>
	  <assert test="(mule:set-variable[@variableName='env'])">AUTH::Authorization code not available</assert>
      <report test="(mule:set-variable[@variableName='env'])">success</report>
	  <assert test="(mule:message-properties-transformer[@doc:name='PolicyValueMapping'])">AUTH::Authorization code not available</assert>
      <report test="(mule:message-properties-transformer[@doc:name='PolicyValueMapping'])">success</report>
	  <assert test="(scripting:transformer[@doc:name='PolicyValidaton'])">AUTH::Authorization code not available</assert>
      <report test="(scripting:transformer[@doc:name='PolicyValidaton'])">success</report>
    </rule> -->
    
	
	<rule context="mule:flow[@name='proxy']">
	  <assert test="(mule:component[@doc:name='PolicyValidation&amp;Security'])">AUTH::Remove Authorization code not available</assert>	    
	  <report test="(mule:component[@doc:name='PolicyValidation&amp;Security'])">success</report>
	  <assert test="count(mule:component)=1">MCOM::Should not have a java compoent</assert>	    
	  <report test="count(mule:component)=1">success</report>
	  <assert test="not(ajax:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ajax:outbound-endpoint)">success</report>
	  <assert test="not(s3:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(s3:no-operation-selected)">success</report>
	  <assert test="not(sqs:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sqs:no-operation-selected)">success</report>
	  <assert test="not(cmis:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(cmis:no-operation-selected)">success</report>
	  <assert test="not(db:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(db:no-operation-selected)">success</report>
	  <assert test="not(ftp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ftp:outbound-endpoint)">success</report>
	  <assert test="not(facebook:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(facebook:no-operation-selected)">success</report>
	  <assert test="not(file:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(file:outbound-endpoint)">success</report>
	  <assert test="not(mule:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(mule:outbound-endpoint)">success</report>
	  <assert test="not(imap:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(imap:outbound-endpoint)">success</report>
	  <assert test="not(jms:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(jms:outbound-endpoint)">success</report>
	  <assert test="not(jetty:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(jetty:outbound-endpoint)">success</report>
	  <assert test="not(magento:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(magento:no-operation-selected)">success</report>
	  <assert test="not(mongo:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(mongo:no-operation-selected)">success</report>
	  <assert test="not(pop3:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(pop3:outbound-endpoint)">success</report>
	  <assert test="not(quartz:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(quartz:outbound-endpoint)">success</report>
	  <assert test="not(rmi:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(rmi:outbound-endpoint)">success</report>
	  <assert test="not(sap:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sap:outbound-endpoint)">success</report>
	  <assert test="not(sftp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sftp:outbound-endpoint)">success</report>
	  <assert test="not(smtp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(smtp:outbound-endpoint)">success</report>
	  <assert test="not(ssl:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ssl:outbound-endpoint)">success</report>
	  <assert test="not(sfdc:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sfdc:no-operation-selected)">success</report>
	  <assert test="not(servlet:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(servlet:outbound-endpoint)">success</report>
	  <assert test="not(tcp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(tcp:outbound-endpoint)">success</report>
	  <assert test="not(twitter:create-place)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(twitter:create-place)">success</report>
	  <assert test="not(udp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(udp:outbound-endpoint)">success</report>
	  <assert test="not(vm:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(vm:outbound-endpoint)">success</report>
	  <assert test="not(wmq:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(wmq:outbound-endpoint)">success</report>
	  <assert test="not(ws:consumer)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ws:consumer)">success</report>
	  <assert test="not(ws:consumer)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ws:consumer)">success</report>
    <!--  <assert test="(mule:flow-ref[@name = 'security-validation'])">AUTH::Remove Authorization code not available</assert>
      <report test="(mule:flow-ref[@name = 'security-validation'])">success</report>
	  <assert test="(mule:message-properties-transformer[@doc:name='SetCredentials'])">AUTH::Remove Authorization code not available</assert>
      <report test="(mule:message-properties-transformer[@doc:name='SetCredentials'])">success</report> -->
    </rule>
	
	<rule context="mule:flow[@name='console']">
	  <assert test="not(mule:component)">MCOM::Should not have a java compoent</assert>	    
	  <report test="not(mule:component)">success</report>
	  <assert test="not(ajax:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ajax:outbound-endpoint)">success</report>
	  <assert test="not(s3:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(s3:no-operation-selected)">success</report>
	  <assert test="not(sqs:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sqs:no-operation-selected)">success</report>
	  <assert test="not(cmis:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(cmis:no-operation-selected)">success</report>
	  <assert test="not(db:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(db:no-operation-selected)">success</report>
	  <assert test="not(ftp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ftp:outbound-endpoint)">success</report>
	  <assert test="not(facebook:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(facebook:no-operation-selected)">success</report>
	  <assert test="not(file:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(file:outbound-endpoint)">success</report>
	  <assert test="not(mule:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(mule:outbound-endpoint)">success</report>
	  <assert test="not(imap:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(imap:outbound-endpoint)">success</report>
	  <assert test="not(jms:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(jms:outbound-endpoint)">success</report>
	  <assert test="not(jetty:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(jetty:outbound-endpoint)">success</report>
	  <assert test="not(magento:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(magento:no-operation-selected)">success</report>
	  <assert test="not(mongo:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(mongo:no-operation-selected)">success</report>
	  <assert test="not(pop3:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(pop3:outbound-endpoint)">success</report>
	  <assert test="not(quartz:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(quartz:outbound-endpoint)">success</report>
	  <assert test="not(rmi:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(rmi:outbound-endpoint)">success</report>
	  <assert test="not(sap:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sap:outbound-endpoint)">success</report>
	  <assert test="not(sftp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sftp:outbound-endpoint)">success</report>
	  <assert test="not(smtp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(smtp:outbound-endpoint)">success</report>
	  <assert test="not(ssl:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ssl:outbound-endpoint)">success</report>
	  <assert test="not(sfdc:no-operation-selected)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(sfdc:no-operation-selected)">success</report>
	  <assert test="not(servlet:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(servlet:outbound-endpoint)">success</report>
	  <assert test="not(tcp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(tcp:outbound-endpoint)">success</report>
	  <assert test="not(twitter:create-place)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(twitter:create-place)">success</report>
	  <assert test="not(udp:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(udp:outbound-endpoint)">success</report>
	  <assert test="not(vm:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(vm:outbound-endpoint)">success</report>
	  <assert test="not(wmq:outbound-endpoint)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(wmq:outbound-endpoint)">success</report>
	  <assert test="not(ws:consumer)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ws:consumer)">success</report>
	  <assert test="not(ws:consumer)">MCON::Connector is not approved for usage</assert>	    
	  <report test="not(ws:consumer)">success</report>
    <!--  <assert test="(mule:flow-ref[@name = 'security-validation'])">AUTH::Remove Authorization code not available</assert>
      <report test="(mule:flow-ref[@name = 'security-validation'])">success</report>
	  <assert test="(mule:message-properties-transformer[@doc:name='SetCredentials'])">AUTH::Remove Authorization code not available</assert>
      <report test="(mule:message-properties-transformer[@doc:name='SetCredentials'])">success</report> -->
    </rule>
	

</pattern>
</schema>
